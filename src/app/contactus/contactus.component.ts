import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home/home.service';
import { MissionService } from '../app.service';

@Component({
  selector: 'contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  constructor(
    private _service: HomeService,
    private _missionService: MissionService
  ) { }

  listContact: any;
  loaderList = false;
  shimmer: any = [];

  ngOnInit() {
    this._missionService.scrollTop(false);
    this.placeHolder();
    this.aboutContent();
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  aboutContent() {
    this._service.contactList()
      .subscribe((res: any) => {
        this.loaderList = true;
        console.log("website/contactUs", res);
        this.listContact = res.data;
      }, err => {
        console.log(err.error);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

}
