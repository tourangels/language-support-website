import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ContactusComponent } from './contactus.component';
import { SharedLazyModule } from '../shared/shared-lazy.module';
const routes: Routes = [  
  { path: '', component: ContactusComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedLazyModule
  ],
  declarations: [
    ContactusComponent
  ]
})
export class ContactusModule { }
