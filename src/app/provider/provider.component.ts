import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ProviderService } from "./provider.service";
import { HomeService } from "../home/home.service";
import { LanguageService } from "../app.language";
import { MissionService } from "../app.service";
import { Configuration } from "../app.constant";
import { Location } from "@angular/common";
import { Paho } from "ng2-mqtt";

declare var $: any;
declare var google: any;
declare var moment: any;
declare var flatpickr: any;

@Component({
  selector: "provider",
  templateUrl: "./provider.component.html",
  styleUrls: ["./provider.component.css"]
})
export class ProviderComponent implements OnInit {
  private _client: Paho.MQTT.Client;

  constructor(
    private _lang: LanguageService,
    private route: ActivatedRoute,
    private _router: Router,
    private _conf: Configuration,
    private _missionService: MissionService,
    private _service: ProviderService,
    private _serviceHome: HomeService,
    private _location: Location
  ) {}

  loaderList = false;
  cityAddress: any;
  catId: any;
  catName: any;
  shimmer: any = [];
  providerList: any;
  providerListCat: any;
  latlng: any;
  ipAddress: any;
  listFilter: any;
  listReview: any;
  listReviews: any = [];
  provideLoader = false;
  filterLoader = false;
  loadMore = false;
  providerId: any;
  providerRw: any;
  pageno = 0;

  keyName: string;
  distance = 0;
  minFees: number;
  maxFees: number;
  subCatId: string;
  bookingType = 1;

  bookinList: any;
  sendDate: any = [];
  sendTime: any = [];
  asapSchedule = false;
  locationLists = false;
  bookingList = false;
  count = 1;
  bookDate: any;
  bookTitle: any;
  otherDate = false;
  scheduleDateTimeStamp: any;
  scheduleTime: any;
  bookDates: any;
  filterLists: any;
  waitForResponse = 1;
  intervals: any;
  providerLang: any;

  ngOnInit() {
    flatpickr(".BtypePicker", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      minDate: moment()
        .add(9, "days")
        .format("YYYY-MM-DD"),
      allowInput: false

      // defaultDate:   moment().format('YYYY-MM-DD HH:mm:ss')
      // dateFormat: "H:i",
      // enableTime: true,
      // minTime: "16:00",
      // minuteIncrement:30,
      // dateFormat: "Y-m-d",
    });
    var nexthour = moment()
      .add(1, "hour")
      .format("HH:mm");
    // console.log(nexthour, moment().format('HH:m'));
    flatpickr(".BtypeTimePicker", {
      // allowInput: true,
      enableTime: true,
      noCalendar: true,
      dateFormat: "h:i K",
      altFormat: "h:i K",
      defaultDate: nexthour,
      minTime: nexthour,
      minuteIncrement: 15
    });
    this.providerLang = this._lang.provider;
    this._missionService.scrollTop(true);
    this.placeHolder();
    this.ipAddress = this._conf.getItem("ip");
    this.route.params.subscribe(params => {
      this.cityAddress = params["city"];
      this.catId = params["id"];
      this.catName = params["name"];
    });
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      let place = JSON.parse(this._conf.getItem("place"));
      $("#latS").val(this.latlng.lat);
      $("#lngS").val(this.latlng.lng);
      $("#changeLocationId").val(place);
    }

    this.getDate();
    this.getTimes();

    if (this._conf.getItem("bookingDates")) {
      var date = JSON.parse(this._conf.getItem("bookingDates"));
      console.log("date", date);
      this.bookTitle = date.bookingType == "1" ? "Book Now" : "Schedule";
      this.bookDate = date.bookDate;
      this.bookingType = date.bookingType;
      this.scheduleDateTimeStamp = date.scheduleDateTimeStamp;
      this.scheduleTime = date.scheduleTime;
    } else {
      this.dateChange();
    }

    this.filterList();
    this.mqttConnect();
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  addHour(i: number) {
    this.count = Math.min(Math.max(1, this.count + i));
  }

  arrow = 1;
  arrowChange(i: number) {
    this.arrow = Math.min(3, Math.max(1, this.arrow + i));
    // console.log(this.arrow)
  }

  backClicked() {
    this._location.back();
  }

  getDate() {
    let daysRequired = 8;
    for (let i = 1; i <= daysRequired; i++) {
      // console.log(this.send_date);
      var date_array = {
        date: "",
        day: "",
        active: false,
        unformat: ""
      };
      if (i == 1) {
        date_array.day = "Tommorrow";
      } else if (i == 0) {
        date_array.day = "Today";
      } else {
        date_array.day = moment()
          .add(i, "days")
          .format("dddd");
      }
      date_array.date = moment()
        .add(i, "days")
        .format("D");

      date_array.unformat = moment()
        .add(i, "days")
        .format("YYYY-MM-DD HH:mm:ss");

      this.sendDate.push(date_array);
      this.sendDate[0].active = true;
      let date = this.sendDate[0].unformat;
      this.bookDate = moment(date).format("dddd,D MMM Y");
    }
    // var asapNow = moment().format('YYYY-MM-DD HH:mm:ss');
    // this._conf.setItem("bookingDate", asapNow);
    // console.log(this.sendDate);
  }

  selectDate(i) {
    // console.log(i)
    if (i == "Ds") {
      this.otherDate = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.otherDate = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[i].active = true;
      // this._conf.setItem("bookingDate", this.sendDate[i].unformat);
      let date = this.sendDate[i].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
    }
  }

  asapSchedules(val) {
    this.otherDate = false;
    if (val == 0) {
      this.asapSchedule = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.asapSchedule = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[0].active = true;
      // console.log(this.sendDate[0].unformat);
      let date = this.sendDate[0].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
      // this._conf.setItem("bookingDate", this.sendDate[0].unformat);
    }
  }

  dateChange() {
    clearInterval(this.intervals);
    if (this.asapSchedule == true) {
      this.bookingType = 2;
      this.bookTitle = "Schedule";
      if (this.otherDate == true) {
        let date =
          $(".BtypePicker").val() ||
          moment()
            .add(9, "days")
            .format("YYYY-MM-DD");
        this.bookDate = moment(date).format("dddd,D MMM Y");
      } else {
        this.bookDate = this.bookDates;
      }
    } else {
      this.bookingType = 1;
      let bookDate = new Date();
      this.bookTitle = "Book Now";
      this.bookDate = moment()
        .add(bookDate, "days")
        .format("dddd,D MMM Y");
    }

    let time =
      $(".BtypeTimePicker").val() ||
      moment()
        .add(1, "hour")
        .format("hh:mm:ss A");
    this.scheduleDateTimeStamp = moment(
      this.bookDate + " " + time,
      "dddd,D MMM Y hh:mm:ss A"
    ).unix();
    this.scheduleTime = this.count * 60;
    // console.log(this.bookDate, this.scheduleDateTimeStamp, this.scheduleTime);

    let list = {
      bookingType: this.bookingType,
      bookDate: this.bookDate,
      bookTime: time,
      bookCount: this.count,
      scheduleDateTimeStamp: this.scheduleDateTimeStamp,
      scheduleTime: this.scheduleTime
    };
    this._conf.setItem("bookingDates", JSON.stringify(list));
    this.filterList();
  }

  typeBooking() {
    // this.sendDate.forEach(x => {
    //   x.active = false;
    // });
    // this.asapSchedule = false;
    this.bookingList = this.providerList.category;
    console.log(this.bookingList);
    $("#notaryTask").modal("show");
  }

  ngOnDestroy() {
    $("body").removeClass("hideHidden");
    this.filterLists = false;
    // console.log("sfgasdfg");
    clearInterval(this.intervals);
    // this.intervals == 2;
    // this.setInterVal();
  }

  filtersList() {
    clearInterval(this.intervals);
    $("body").toggleClass("hideHidden");
    this.filterLists = !this.filterLists;
    this.pageno = 0;
  }

  // function for displaying time
  getTimes() {
    let hoursRequired = 46;
    for (let i = 0; i <= hoursRequired; i++) {
      var hour_array = {
        hour: "",
        nexthour: ""
      };
      hour_array.hour = moment()
        .add(i, "hour")
        .format("h A");
      var curr = moment()
        .add(i, "hour")
        .format("h h");
      var current_hour = i;
      current_hour++;
      hour_array.nexthour = moment()
        .add(current_hour, "hour")
        .format("h A");
      this.sendTime.push(hour_array);
    }
  }

  selectProviders() {
    this.selectProvider(this.providerId);
  }

  selectProvider(id) {
    this._conf.removeItem("backClicked");
    var loginFlag = this._conf.getItem("isLogin");
    if (loginFlag == "true") {
      this._router.navigate([
        "./",
        this.cityAddress,
        this.catName,
        this.catId,
        id
      ]);
    } else {
      this._missionService.loginOpen(false);
    }
  }

  subCat(index) {
    $(".sideListLi").removeClass("active");
    $("#" + index + "sCat").addClass("active");
    this.subCatId = this.providerList.subCategoryData[index]._id;
  }

  searchProvider(val) {
    clearInterval(this.intervals);
    // let keyName = val.length > 0 || "";
    this.keyName = val;
    this.filterList();
  }

  distanceList(val) {
    clearInterval(this.intervals);
    this.distance = Number(val);
    var input = $(".slider_range").val();
    var value =
      (input - $(".slider_range").attr("min")) /
      ($(".slider_range").attr("max") - $(".slider_range").attr("min"));
    $(".slider_range").css(
      "background-image",
      "-webkit-gradient(linear, left top, right top, " +
        "color-stop(" +
        value +
        ", #01b5f6), " +
        "color-stop(" +
        value +
        ", #C5C5C5)" +
        ")"
    );
  }

  filterContentList() {
    clearInterval(this.intervals);
    this.filterLoader = true;
    if (this.providerList && this.providerList.category) {
      let min = this.providerList.category.minimum_fees;
      let max = this.providerList.category.miximum_fees;
    }
    this.waitForResponse = 1;
    this.pageno = 1;
    this.filterList();
  }

  errMsg: any;
  ErrMsg: any;
  minPrice(price, maxpe) {
    clearInterval(this.intervals);
    this.errMsg = false;
    // setTimeout(() => {
    if (maxpe < this.minFees) {
      this.minFees = maxpe;
      this.errMsg = "maximum price should be " + maxpe;
    } else {
      if (price > this.minFees) {
        this.minFees = price;
        this.errMsg = "minimum price should be " + price;
      }
    }
    setTimeout(() => {
      this.errMsg = false;
    }, 1500);

    // }, 2000)

    // console.log(min, inP, this.minFees)
  }

  maxPrice(price, maxpe) {
    clearInterval(this.intervals);
    this.errMsg = false;
    // setTimeout(() => {
    if (maxpe < this.maxFees) {
      this.maxFees = maxpe;
      this.errMsg = "maximum price should be " + maxpe;
    } else {
      if (price > this.maxFees) {
        this.maxFees = price;
        this.errMsg = "minimum price should be " + price;
      }
    }
    setTimeout(() => {
      this.errMsg = false;
    }, 1500);

    // }, 2000)
    // console.log(this.maxFees)
  }

  filterList() {
    this.ErrMsg = false;
    let lat = $("#latS").val();
    let lng = $("#lngS").val();
    let list = {
      lat: lat,
      long: lng,
      ip: this.ipAddress,
      categoryId: this.catId,
      search: this.keyName || "0",
      bookingType: this.bookingType,
      subCategoryId: this.subCatId || "0",
      distance: this.distance || 0,
      scheduleDateTimeStamp: "",
      scheduleTime: "",
      minFees: this.minFees || 0,
      maxFees: this.maxFees || 0,
      waitForResponse: this.waitForResponse
    };
    if (this.bookingType == 2) {
      list.scheduleDateTimeStamp = this.scheduleDateTimeStamp;
      list.scheduleTime = this.scheduleTime;
    }
    if (this.waitForResponse == 1) {
      this.provideLoader = true;
    }
    this._service.providerFilter(list).subscribe(
      (res: any) => {
        $("body").removeClass("hideHidden");
        this.filterLists = false;
        console.log("website/provider/filter", res);
        if (
          res.data &&
          res.data.category &&
          res.data.category.service_type == "1"
        ) {
          this._router.navigate([""]);
        } else {
          if (this.waitForResponse == 1) {
            this.filterLoader = false;
            this.provideLoader = false;
            this.loaderList = true;
            // console.log("this.pageno", this.pageno)
            if (this.pageno == 0) {
              this.providerListCat = res.data;
            }
            this.providerList = res.data;
            if (
              res.data.category.service_type == "2" &&
              res.data.category.billing_model != "5" &&
              this.pageno == 0
            ) {
              this.minFees = res.data.category.minimum_fees;
              this.maxFees = res.data.category.miximum_fees;
            }
            res.data.provider.forEach(x => {
              x.price = parseFloat(x.price).toFixed(2);
            });
            res.data.provider.sort(a => {
              if (a.status == 1) {
                return -1;
              } else {
                return 1;
              }
            });
            this.timeFunction();
            this.setInterVal();
          }
        }
      },
      err => {
        this.filterLoader = false;
        this.provideLoader = false;
        this.loaderList = true;
        console.log(err.error.message);
        this.ErrMsg = err.error.message;
        setTimeout(() => {
          this.ErrMsg = false;
        }, 3000);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
          // var currentLocation = window.location;
          // this._conf.setItem("pathname", currentLocation.pathname);
          // this._router.navigate(['']);
        }
      }
    );
  }

  timeFunction() {
    var len = this.providerList.provider;
    for (var i = 0; i < len.length; i++) {
      if (this.providerList.provider[i].review[0]) {
        var date = this.providerList.provider[i].review[0].reviewAt;
        var post = date;
        var poston = Number(post);
        var postdate = new Date(poston * 1000);
        var currentdate = new Date();
        var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
        var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);
        if (1 < diffDays) {
          this.providerList.provider[i].review[0].reviewAt =
            diffDays + " Days ago";
        } else if (1 <= diffHrs) {
          this.providerList.provider[i].review[0].reviewAt =
            diffHrs + " Hours ago";
        } else {
          this.providerList.provider[i].review[0].reviewAt =
            diffMins + " Minutes ago";
        }
      }
    }
  }

  setInterVal() {
    this.intervals = setInterval(() => {
      console.log("waitForResponse", this.waitForResponse);
      // if (this.waitForResponse == 2) {
      //   clearInterval(this.intervals);
      // } else {
      this.waitForResponse = 0;
      this.filterList();
      // }
    }, 10000);
  }

  mqttConnect() {
    var id = this._conf.getItem("sid");
    this._client = new Paho.MQTT.Client(
      "mqtt.tourangels.it",
      2083,
      "/mqtt",
      id
    );

    this._client.onConnectionLost = (responseObject: Object) => {
      console.log("Connection lost.");
    };

    this._client.onMessageArrived = (message: Paho.MQTT.Message) => {
      console.log("Message arrived.", JSON.parse(message.payloadString));
      let statusList = JSON.parse(message.payloadString);
      this.providerList = statusList.data;
      if (
        statusList.data.category.service_type == "2" &&
        statusList.data.category.billing_model != "5" &&
        this.pageno == 0
      ) {
        this.minFees = statusList.data.category.minimum_fees;
        this.maxFees = statusList.data.category.miximum_fees;
      }
      statusList.data.provider.forEach(x => {
        x.price = parseFloat(x.price).toFixed(2);
      });
      statusList.data.provider.sort(a => {
        if (a.status == 1) {
          return -1;
        } else {
          return 1;
        }
      });
      this.timeFunction();
    };

    this._client.connect({ onSuccess: this.onConnected.bind(this) });
  }

  private onConnected(): void {
    var cId = this._conf.getItem("sid");
    this._client.subscribe("provider/" + cId, { qos: 2 });
    console.log("Connected to broker provider.");
  }

  reviewList(id) {
    var catName = this.catName.replace(/[^A-Z0-9]/gi, "_");
    this._router.navigate([
      "./review",
      this.cityAddress,
      catName,
      this.catId,
      id
    ]);
  }

  cancelField() {
    $("#changeLocationId").val(" ");
  }

  changeLocation() {
    var input = document.getElementById("changeLocationId");
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener("place_changed", () => {
      var place = autocomplete.getPlace();
      $("#latS").val(place.geometry.location.lat());
      $("#lngS").val(place.geometry.location.lng());
    });
  }
}
