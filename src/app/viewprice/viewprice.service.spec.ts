import { TestBed, inject } from '@angular/core/testing';

import { ViewpriceService } from './viewprice.service';

describe('ViewpriceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewpriceService]
    });
  });

  it('should be created', inject([ViewpriceService], (service: ViewpriceService) => {
    expect(service).toBeTruthy();
  }));
});
