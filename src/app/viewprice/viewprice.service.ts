import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Configuration } from '../app.constant';



@Injectable({
  providedIn: 'root'
})

export class ViewpriceService {


  constructor(private _http: HttpClient, private _conf: Configuration) {
  }

  viewServiceList(list) {
    this._conf.setAuth();
    return this._http.get(this._conf.CustomerUrl + 'services/' + list.catId + "/" + list.providerId, { headers: this._conf.headers });
  }


  getCart(list) {
    console.log(list)
    this._conf.setAuth();
    return this._http.get(this._conf.CustomerUrl + 'cart/' + list.catId + "/" + list.providerId, { headers: this._conf.headers });
  }


  addCart(list) {
    console.log(list)
    let body = list;
    this._conf.setAuth();
    return this._http.post(this._conf.CustomerUrl + 'cart', body, { headers: this._conf.headers });
  }

}
