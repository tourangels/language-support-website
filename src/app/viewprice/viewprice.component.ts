import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewpriceService } from './viewprice.service';
import { HomeService } from '../home/home.service';
import { LanguageService } from '../app.language';
import { MissionService } from '../app.service';
import { Configuration } from '../app.constant';
import { Location } from '@angular/common';

declare var $: any;
declare var moment: any;
declare var flatpickr: any;

@Component({
  selector: 'viewprice',
  templateUrl: './viewprice.component.html',
  styleUrls: ['./viewprice.component.css']
})
export class ViewpriceComponent implements OnInit {

  constructor(
    private _lang: LanguageService,
    private route: ActivatedRoute,
    private _router: Router,
    private _conf: Configuration,
    private _missionService: MissionService,
    private _service: ViewpriceService,
    private _serviceHome: HomeService,
    private _location: Location,
  ) { }

  catId: any;
  catName: any;
  providerId: any;
  serviceList: any;
  cartList: any;
  action = 1;
  catSearch: any;
  subCatList: any;
  providerList: any;
  cartListActive: any;
  qtyAllow: any;
  descList: any;
  serviceProvider = false;
  ratingTrue = false;
  shimmer: any = [];
  loaderList = false;
  serviceType: number;
  serviceTypehr: any;

  service_type: any;
  billing_model: any;
  hourlyList: any;
  fixedList: any;
  cartCount: any;

  bookinList: any;
  sendDate: any = [];
  sendTime: any = [];
  asapSchedule = false;
  locationLists = false;
  bookingList = false;
  bookingType: any;
  count = 1;
  bookDate: any;
  bookTitle: any;
  otherDate = false;
  scheduleDateTimeStamp: any;
  scheduleTime: any;
  bookDates: any;
  viewLang:any;
  ngOnInit() {

    flatpickr('.BtypePicker', {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: 'Y-m-d',
      minDate: moment().add(9, 'days').format('YYYY-MM-DD'),
      allowInput: false,
    });
    var nexthour = moment().add(1, 'hour').format('HH:mm');
    // console.log(nexthour, moment().format('h:m'));
    flatpickr('.BtypeTimePicker', {
      // allowInput: true,
      enableTime: true,
      noCalendar: true,
      dateFormat: "h:i K",
      altFormat: 'h:i K',
      defaultDate: nexthour,
      minTime: nexthour,
      minuteIncrement: 15,
    });
    this.viewLang = this._lang.viewPrice;
    this._missionService.scrollTop(true);
    this.placeHolder();
    let back = this._conf.getItem("backClicked");
    this.route.params.subscribe(params => {
      this.catId = params['id'];
      this.catName = params['name'];
      this.providerId = params['Pid'];
      if (back == '1') {
        if (this.providerId != '0') {
          this._location.back();
        } else {
          this._router.navigate([""]);
        }
        this._conf.removeItem("backClicked");
      } else {
        this.listService(this.catId);
        this.getDate();
        this.getTimes();
        if (this._conf.getItem('bookingDates')) {
          var date = JSON.parse(this._conf.getItem('bookingDates'));
          console.log("date", date);
          this.bookTitle = date.bookingType == "1" ? "Book Now" : "Schedule";
          this.bookDate = date.bookDate
          this.bookingType = date.bookingType;
          this.scheduleDateTimeStamp = date.scheduleDateTimeStamp;
          this.scheduleTime = date.scheduleTime;
        } else {
          this.dateChange();
        }
      }
    });

  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  addHour(i: number) {
    this.count = Math.min(Math.max(1, this.count + i));
  }

  arrow = 2;
  arrowChange(i: number) {
    this.arrow = Math.min(3, Math.max(1, this.arrow + i));
    // console.log(this.arrow)
  }

  backClicked() {
    if (this.providerId != '0') {
      this._location.back();
    } else {
      this._router.navigate([""]);
    }
  }

  getDate() {
    let daysRequired = 8;
    for (let i = 1; i <= daysRequired; i++) {
      // console.log(this.send_date);
      var date_array = {
        "date": "",
        "day": "",
        "active": false,
        "unformat": "",
      };
      if (i == 1) {
        date_array.day = "Tommorrow"
      } else if (i == 0) {
        date_array.day = "Today"
      } else {
        date_array.day = moment().add(i, 'days').format('dddd');
      }
      date_array.date = moment().add(i, 'days').format('D');

      date_array.unformat = moment().add(i, 'days').format('YYYY-MM-DD HH:mm:ss');

      this.sendDate.push(date_array);
      this.sendDate[0].active = true;
      let date = this.sendDate[0].unformat;
      this.bookDate = moment(date).format('dddd,D MMM Y');
    }
    // var asapNow = moment().format('YYYY-MM-DD HH:mm:ss');
    // this._conf.setItem("bookingDate", asapNow);
    // console.log(this.sendDate);
  }

  selectDate(i) {
    // console.log(i)
    if (i == "Ds") {
      this.otherDate = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.otherDate = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[i].active = true;
      // this._conf.setItem("bookingDate", this.sendDate[i].unformat);     
      let date = this.sendDate[i].unformat;
      this.bookDates = moment(date).format('dddd,D MMM Y');
    }
  }

  asapSchedules(val) {
    this.otherDate = false;
    if (val == 0) {
      this.asapSchedule = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.asapSchedule = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[0].active = true;
      // console.log(this.sendDate[0].unformat);
      let date = this.sendDate[0].unformat;
      this.bookDates = moment(date).format('dddd,D MMM Y');
      // this._conf.setItem("bookingDate", this.sendDate[0].unformat);
    }
  }

  dateChange() {
    if (this.asapSchedule == true) {
      this.bookingType = 2;
      this.bookTitle = "Schedule";
      if (this.otherDate == true) {
        let date = $(".BtypePicker").val() || moment().add(9, 'days').format('YYYY-MM-DD');
        this.bookDate = moment(date).format('dddd,D MMM Y');
      } else {
        this.bookDate = this.bookDates;
      }
    } else {
      this.bookingType = 1;
      let bookDate = new Date();
      this.bookTitle = "Book Now";
      this.bookDate = moment().add(bookDate, 'days').format('dddd,D MMM Y');
    }

    let time = $(".BtypeTimePicker").val() || moment().add(1, 'hour').format('hh:mm:ss A');
    this.scheduleDateTimeStamp = moment(this.bookDate + " " + time, 'dddd,D MMM Y hh:mm:ss A').unix();
    this.scheduleTime = this.count * 60;
    // console.log(this.bookDate, this.scheduleDateTimeStamp, this.scheduleTime);

    let list = {
      bookingType: this.bookingType,
      bookDate: this.bookDate,
      bookTime: time,
      bookCount: this.count,
      scheduleDateTimeStamp: this.scheduleDateTimeStamp,
      scheduleTime: this.scheduleTime,
    }
    this._conf.setItem('bookingDates', JSON.stringify(list));
  }

  typeBooking() {
    this.sendDate.forEach(x => {
      x.active = false;
    });
    this.asapSchedule = false;
    this.bookingList = this.subCatList;
    console.log(this.bookingList);
    $("#notaryTask").modal("show");
  }

  // function for displaying time
  getTimes() {
    let hoursRequired = 46;
    for (let i = 0; i <= hoursRequired; i++) {
      var hour_array = {
        "hour": "",
        "nexthour": "",
      };
      hour_array.hour = moment().add(i, 'hour').format('h A');
      var curr = moment().add(i, 'hour').format('h h');
      var current_hour = i;
      current_hour++;
      hour_array.nexthour = moment().add(current_hour, 'hour').format('h A');
      this.sendTime.push(hour_array);
    }
  }


  listService(id) {
    let list = {
      catId: id,
      providerId: this.providerId
    }
    this.loaderList = false;
    this._service.viewServiceList(list)
      .subscribe((res: any) => {
        console.log("customer/services", res);
        this.serviceList = res.data;
        this.subCatList = res.categoryData;
        this.providerList = res.providerData;
        this.subCatList.btnActive = true;
        this.subCatList.loader = false;
        this.service_type = this.subCatList.service_type;
        this.billing_model = this.subCatList.billing_model;
        console.log(this.service_type, this.billing_model);
        if (this.billing_model != 3 && this.billing_model != 4) {
          this.loaderList = true;
        }
        for (var i = 0; i < this.serviceList.length; i++) {
          this.serviceList[i].service.forEach(x => {
            x.btnActive = true;
            x.qty = x.quantity;
          });
        }
        this.getCartList(this.catId);
      }, err => {            
        console.log(err.error.message)
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }        
      });
  }


  getCartList(id) {
    let list = {
      catId: id,
      providerId: this.providerId
    }
    this._service.getCart(list)
      .subscribe((res: any) => {
        console.log("customer/get-cart", res);
        this.cartList = res.data;
        this.serviceTypehr = res.data.serviceType;
        if (this.billing_model == 3 || this.billing_model == 4) {
          if (res.data.totalQuntity == 0) {
            this.addCart('1', 1, 0, 0);
          } else {
            var loginFlag = this._conf.getItem("isLogin");
            if (loginFlag == "true") {
              this.checkOut();
            } else {
              this._missionService.loginOpen(false);
            }
          }
        }
        // this.listService(this.catId);
        var cartActive = false;
        var len = [];
        this.cartList.item.forEach(x => {
          if (x.quntity > 0) {
            var serviceId = x.serviceId;
            if (this.cartList.serviceType == 1) {
              for (var i = 0; i < this.serviceList.length; i++) {
                var index = this.serviceList[i].service.findIndex(x => x._id == serviceId);
                if (index > -1) {
                  this.serviceList[i].service[index].btnActive = false;
                  this.serviceList[i].service[index].qty = x.quntity;
                  this.subCatList.btnActive = true;
                }
              }
            } else {
              this.subCatList.btnActive = false;
              this.subCatList.qty = x.quntity;
              for (var i = 0; i < this.serviceList.length; i++) {
                this.serviceList[i].service.forEach(x => {
                  x.btnActive = true;
                });
              }
            }
            len.push(x);
            this.cartCount = len.length;
            cartActive = true;
          } else {
            var serviceId = x.serviceId;
            if (this.cartList.serviceType == 1) {
              for (var i = 0; i < this.serviceList.length; i++) {
                var index = this.serviceList[i].service.findIndex(x => x._id == serviceId);
                if (index > -1) {
                  this.serviceList[i].service[index].btnActive = true;
                  this.serviceList[i].service[index].qty = x.quntity;
                  this.subCatList.btnActive = true;
                }
              }
            } else {
              this.subCatList.btnActive = true;
              this.subCatList.qty = x.quntity;
              for (var i = 0; i < this.serviceList.length; i++) {
                this.serviceList[i].service.forEach(x => {
                  x.btnActive = true;
                });
              }
            }
          }
        });

        if (cartActive) {
          this.cartListActive = true;
        } else {
          this.cartListActive = false;
        }
      }, err => {
        this.cartCount = 0;
        var error = err.error;
        console.log(error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
        if ((err.status == 416) && (this.billing_model == 3 || this.billing_model == 4)) {
          this.addCart('1', 1, 0, 0);
        }
      });
  }

  addMinusCart(id, action, index) {
    var maxQty = this.cartList.item[index].maxquantity;
    var Qty = this.cartList.item[index].quntity;
    if ((action == 1) && (maxQty > Qty) || (action == 2) && (maxQty >= Qty) || (action == 0) || this.cartList.serviceType == 2) {
      this.action = action;
      this.serviceType = this.cartList.serviceType;
      this.addCartList(id);
      this.cartList.item[index].loader = true;
      setTimeout(() => {
        // if (action == 1) {
        //   this.cartList.item[index].quntity += 1;
        // } else {
        //   this.cartList.item[index].quntity -= 1;
        // }
        this.cartList.item[index].loader = false;
      }, 500);
    } else {
      this.cartList.item[index].maxqty = true;
      setTimeout(() => {
        this.cartList.item[index].maxqty = false;
      }, 1500);
    }
  }

  addCart(id, action, i, index) {
    var loginFlag = this._conf.getItem("isLogin");
    if (loginFlag) {
      if (this.serviceList && this.serviceList.length > 0) {
        var maxQty = this.serviceList[i].service[index].maxquantity;
        var Qty = this.serviceList[i].service[index].qty;
        var QtyAction = Number(this.serviceList[i].service[index].quantity);
      }
      if ((action == 1) && (maxQty > Qty) || (action == 1) && (QtyAction == 0) || (action == 2) && (maxQty >= Qty) || (action == 0) || id == '1') {
        if (id == '1') {
          this.serviceType = 2;
          this.subCatList.loader = true;
          setTimeout(() => {
            this.subCatList.loader = false;
          }, 1500);
        } else {
          this.serviceType = 1;
          this.serviceList[i].service[index].loader = true;
          setTimeout(() => {
            this.serviceList[i].service[index].loader = false;
          }, 1500);
        }
        this.action = action;
        this.addCartList(id);
      } else {
        this.serviceList[i].service[index].maxqty = true;
        setTimeout(() => {
          this.serviceList[i].service[index].maxqty = false;
        }, 1500);
      }
    } else {
      this._missionService.loginOpen(false);
    }

  }

  addCartList(id) {
    let list = {
      serviceType: this.serviceType,
      bookingType: this.bookingType,
      categoryId: this.catId,
      providerId: this.providerId,
      serviceId: id,
      quntity: 1,
      action: this.action
    }
    if (this.action == 0) {
      list.action = 2;
    }
    this._service.addCart(list)
      .subscribe((res: any) => {
        console.log("customer/cart", res);
        if (this.billing_model == 3 || this.billing_model == 4) {
          this.checkOut();
        } else {
          // this.cartList = res.data;
          this.getCartList(this.catId);
        }
      }, err => {
        var error = err.error;
        console.log(error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

  reviewList(id) {
    let city = this.subCatList.city;
    var catName = this.catName.replace(/[^A-Z0-9]/ig, "_");
    this._router.navigate(["./review", city, catName, this.catId, id]);
  }


  descriptionList(list) {
    this.descList = list;
    $("#descList").modal("show");
  }

  checkOut() {
    this._router.navigate(["./checkout", this.catId, this.providerId]);
  }

}
