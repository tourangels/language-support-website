import { Component } from '@angular/core';
import {Router, NavigationEnd} from "@angular/router";
import {GoogleAnalyticsEventsService} from "./google-analytics-events.service";
import { TranslateService, LangChangeEvent } from "@ngx-translate/core";
import { Configuration} from "./app.constant";
import { Location } from '@angular/common';

declare var ga: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor( private _conf:Configuration,
    public router: Router, public googleAnalyticsEventsService: GoogleAnalyticsEventsService, public translate: TranslateService) {
    this.router.events.subscribe(event => {
      // console.log("ga", event)
      if (event instanceof NavigationEnd) {
        ga('set', 'page', event.urlAfterRedirects);
        ga('send', 'pageview');
      }
    });
  }
  ngOnInit() {}

  translates(val) {
    // console.log("val", val);
    this.translate.use(val);
    this._conf.setItem("lang",val);
    // console.log(this.location.pathname)
    location.reload()
    // window.location.reload();
  }

}
