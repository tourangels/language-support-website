import { Component, OnInit } from '@angular/core';
import { MissionService } from '../app.service';

@Component({
  selector: 'terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  constructor(private _missionService: MissionService) { }

  ngOnInit() {
    this._missionService.scrollTop(false);
  }

}
