import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TermsComponent } from './terms.component';
import { SharedLazyModule } from '../shared/shared-lazy.module';
const routes: Routes = [  
  { path: '', component: TermsComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedLazyModule  
  ],
  declarations: [
    TermsComponent
  ]
})
export class TermsModule { }
