import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home/home.service';
import { MissionService } from '../app.service';
import { Configuration } from '../app.constant';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(
    private _serviceHome: HomeService,
    private _missionService: MissionService,
    private _conf: Configuration,
    private _router: Router
  ) {
    _missionService.catSearchOpen$.subscribe(
      val => {
        this.categoriesList(val);
      })
    _missionService.scrollTop$.subscribe(
      val => {
        this.footerShow = val;
      })   
  }

  catSearch: any;
  ipAddress: any;
  latlng: any;
  ipList: any;
  footerShow: any;
  licenseId: number = 4711811;

  ngOnInit() {
    this.getIp();
  }

  getIp() {
    this._serviceHome.getIpAddress()
      .subscribe((res: any) => {
        this.ipAddress = res.ip;
        // let ipList = res.loc.split(",");
        this.ipList = {
          lat: res.latitude,
          lng: res.longitude
        };
        this.categoriesList("0");
      });
  }

  categoriesList(val) {
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
    } else {
      this.latlng = {
        lat: parseFloat(this.ipList.lat),
        lng: parseFloat(this.ipList.lng)
      }
    }
    let list = {
      lat: this.latlng.lat,
      long: this.latlng.lng,
      ipAddress: this.ipAddress,
      cityId: val,
    }
    this._serviceHome.catList(list, "")
      .subscribe((res: any) => {
        this.catSearch = [];
        console.log("categories", res)
        let catSearch = res.data.catArr;
        catSearch.forEach(x => {
          x.category.forEach(y => {
            // console.log("cat_name", y.cat_name)
            this.catSearch.push(y)
          });
        });
        // console.log(this.catSearch)
      }, err => {
        console.log(err._body);
      });
  }

  typeBooking(list) {
    this._router.navigate([""]);
    // if (!this.footerShow) {
    setTimeout(() => {
      this._missionService.catSearchPop(list);
    }, 200)

    // }
  }

}
