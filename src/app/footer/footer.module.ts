import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { FooterComponent } from "./footer.component";
import { SharedLazyModule } from "../shared/shared-lazy.module";

const routes: Routes = [{ path: "", component: FooterComponent }];

@NgModule({
  imports: [CommonModule, SharedLazyModule, RouterModule.forChild(routes)],
  declarations: [
    // FooterComponent
  ],
  // exports: [FooterComponent]
})
export class FooterModule {}