import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about.component';
import { SharedLazyModule } from '../shared/shared-lazy.module';
const routes: Routes = [  
  { path: '', component: AboutComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedLazyModule,
    RouterModule.forChild(routes)    
  ],
  declarations: [
    AboutComponent
  ]
})
export class AboutModule { }
