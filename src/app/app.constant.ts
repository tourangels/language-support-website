import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Cookie } from 'ng2-cookies';


@Injectable({
    providedIn: 'root'
})

export class Configuration {
    Server: string = 'https://api.tourangels.it/';

    Customer: string = 'customer/';
    Business: string = 'business/';
    Website: string = 'website/';
    Provider: string = 'provider/';
    Url = this.Server;
    CustomerUrl = this.Server + this.Customer;
    BusinessUrl = this.Server + this.Business;
    WebsiteUrl = this.Server + this.Website;
    ProviderUrl = this.Server + this.Provider;
    headers: any;
    support: boolean;
    // token = localStorage.getItem('user') || '';
    // const httpOptions = {
    //     headers: new HttpHeaders({
    //       'Content-Type':  'application/json',
    //       'Authorization': 'my-auth-token'
    //     })
    // };   


    setHeaders(token) {
        // alert(token);
        this.headers = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('lan', 'en')
            .set('authorization', token || '');
    }

    constructor() {
        var storage = (typeof window.localStorage === 'undefined') ? undefined : window.localStorage,
            supported = !(typeof storage === undefined || typeof window === undefined);
        this.support = supported || false;
    }



    setAuth() {
        var token = this.getItem("sessionToken");
        this.setHeaders(token);
    }

    /** storage list **/

    setItem(item, val) {
        if (!this.support) {
            Cookie.set(item, val, 1, "/");
        } else {
            localStorage.setItem(item, val)
        }
    }

    getItem(item) {
        if (!this.support) {
            return Cookie.get(item);
        } else {
            return localStorage.getItem(item)
        }
    }

    removeItem(item) {
        if (!this.support) {
            Cookie.delete(item, "/");
        } else {
            localStorage.removeItem(item)
        }
    }

    clear() {
        if (!this.support) {
            Cookie.deleteAll("/");
        } else {
            localStorage.clear()
        }
    }


}