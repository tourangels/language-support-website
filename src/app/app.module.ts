import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule,HttpClient } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { NgxCarouselModule } from 'ngx-carousel';
import 'hammerjs';
import { LivechatWidgetModule } from '@livechat/angular-widget';
import { Angular2SocialLoginModule } from "angular2-social-login";
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LazyLoadImageModule } from 'ng-lazyload-image';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from "./shared/shared.module";
// import { ViewpriceComponent } from './viewprice/viewprice.component';
// import { CheckoutComponent } from './checkout/checkout.component';
// import { ProfileComponent } from './profile/profile.component';
// import { ProviderComponent } from './provider/provider.component';
// import { BecomeaproComponent } from './becomeapro/becomeapro.component';
// import { AboutComponent } from './about/about.component';
// import { PrivacyComponent } from './privacy/privacy.component';
// import { TermsComponent } from './terms/terms.component';
// import { ContactusComponent } from './contactus/contactus.component';
// import { ReviewComponent } from './review/review.component';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

let providers = {
  "google": {
    "clientId": "493170905947-uo9iec27nh5gul30o4j7g7epgr9vkfkg.apps.googleusercontent.com"
  },
  // "linkedin": {
  //   "clientId": "LINKEDIN_CLIENT_ID"
  // },
  "facebook": {
    "clientId": "199985174131350",
    "apiVersion": "v2.11" //like v2.4 
  }
};

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule' },
  { path: 'becomeatasker', loadChildren: './becomeapro/becomeapro.module#BecomeaproModule' },
  { path: 'terms', loadChildren: './terms/terms.module#TermsModule' },
  { path: 'privacy', loadChildren: './privacy/privacy.module#PrivacyModule' },
  { path: 'about', loadChildren: './about/about.module#AboutModule' },
  { path: 'contact-us', loadChildren: './contactus/contactus.module#ContactusModule' },
  { path: 'review/:city/:name/:id/:Pid', loadChildren: './review/review.module#ReviewModule' },
  { path: 'checkout/:id/:Pid', loadChildren: './checkout/checkout.module#CheckoutModule' },
  { path: ':city/:name/:id/:Pid', loadChildren: './viewprice/viewprice.module#ViewpriceModule' },
  { path: ':city/:name/:id', loadChildren: './provider/provider.module#ProviderModule' },
  { path: '**', component: HomeComponent },
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    // ViewpriceComponent,
    // CheckoutComponent,
    // ProfileComponent,
    // ProviderComponent,
    // BecomeaproComponent,
    // AboutComponent,
    // PrivacyComponent,
    // TermsComponent,
    // ContactusComponent,
    // ReviewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxCarouselModule,
    HttpClientModule,
    Angular2SocialLoginModule,
    LazyLoadImageModule,
    TagInputModule,
    LivechatWidgetModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot(),
    RouterModule.forRoot(routes),
    SharedModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      }
    }),
  ],
  providers: [TranslateModule],
  bootstrap: [AppComponent]
})
export class AppModule { }

Angular2SocialLoginModule.loadProvidersScripts(providers);
