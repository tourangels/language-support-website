import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Configuration } from './app.constant';
import { Paho } from 'ng2-mqtt';

@Injectable({
    providedIn: 'root'
})

export class MissionService {

    // cityId: any;
    // public _client: Paho.MQTT.Client;

    constructor(private _conf: Configuration) {
        // var id = this._conf.getItem('sessionToken');
        // this._client = new Paho.MQTT.Client("mqtt.go-tasker.com", 2083, "/mqtt", id);
    }
    // Observable string sources      
    private catSearchSource = new Subject<boolean>();
    private catSearchPopSource = new Subject<boolean>();
    private loginOpenSource = new Subject<boolean>();
    private scrollTopSource = new Subject<boolean>();
    private scrollTopsSource = new Subject<boolean>();
    private addWalletsSource = new Subject<boolean>();
   


    // Observable string streams   
    catSearchOpen$ = this.catSearchSource.asObservable();
    catSearchPopOpen$ = this.catSearchPopSource.asObservable();
    loginOpen$ = this.loginOpenSource.asObservable();
    scrollTop$ = this.scrollTopSource.asObservable();
    scrollTops$ = this.scrollTopsSource.asObservable();
    addWallets$ = this.addWalletsSource.asObservable();



    // Service message commands     

    catSearch(value) {
        this.catSearchSource.next(value);
    }

    catSearchPop(value) {
        this.catSearchPopSource.next(value);
    }

    loginOpen(value) {
        this.loginOpenSource.next(value);
    }

    scrollTop(value) {
        this.scrollTopSource.next(value);
    }

    scrollTops(value) {
        this.scrollTopsSource.next(value);
    }

    addWallets(value) {
        this.addWalletsSource.next(value);
    }

   



    // connection() {       
    //     console.log('Connected to broker.', this._client);
    //     this._client.connect({            
    //         onSuccess: this.onConnected.bind(this)
    //     });
    // }

    // connectionLost() {
    //     let observable = new Observable(observer => {
    //         this._client.onConnectionLost = (responseObject: Object) => {
    //             console.log("onConnectionLost", responseObject); observer.next(responseObject);
    //         };
    //     })
    //     return observable;
    // }

    // getMessages() {
    //     let observable = new Observable(observer => {
    //         this._client.onMessageArrived = (message: any) => {
    //             observer.next(message);
    //         };
    //         return () => {
    //             this._client.disconnect();
    //         };
    //     })
    //     return observable;
    // }

    // onConnected(): void {
    //     // this.cityId = sessionStorage.getItem("cityId");
    //     // if (this.cityId === "0") {
    //     //     this._client.subscribe("cities/#", {
    //     //         qos: 2
    //     //     });
    //     //     console.log('Connected to broker all city.');
    //     // } else {
    //     //     this._client.subscribe("cities/" + this.cityId + "/#", {
    //     //         qos: 2
    //     //     });
    //     //     console.log('Connected to broker cityid.');
    //     // }
    //     // this._client.subscribe("cities/"+cityId+"/dispatcher/provider/providerLocation/#", { qos: 0 });  
    //     // this._client.subscribe("cities/"+cityId+"/dispatcher/provider/providerStatus/#", { qos: 2 }); 
    //     // this._client.subscribe("cities/"+cityId+"/dispatcher/dispatcher/bookingStatus",{ qos: 2 });  
    //     // this._client.subscribe("dispatcher/booking",{ qos: 2 });  
    //     // this._client.subscribe("provider/" + sessionStorage.getItem('loginUser'), { qos: 2 });   
    //     console.log("this._client", this._client);
    //     console.log('Connected to broker.');
    // }

}




