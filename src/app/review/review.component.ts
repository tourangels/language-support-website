import { Component, OnInit, HostListener, Inject, NgZone, ViewChild, AfterViewInit } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { ProviderService } from '../provider/provider.service';
import { HomeService } from '../home/home.service';
import { LanguageService } from '../app.language';
import { MissionService } from '../app.service';
import { Configuration } from '../app.constant';
import { Location } from '@angular/common';

declare var $: any;

@Component({
  selector: 'review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _conf: Configuration,
    private _missionService: MissionService,
    private _service: ProviderService,
    private _serviceHome: HomeService,
    private _location: Location,
    private _lang:LanguageService
  ) { }

  loadMore = false;
  providerId: any;
  providerRw: any;
  pageno = 0;
  loaderList: any;
  listReview: any;
  listReviews: any = [];
  catId: any;
  catName: any;
  cityAddress: any;
  shimmer: any = [];
  latlng: any;
  headerrestOpen: any;
  reviewLang:any;


  ngOnInit() {
    this.reviewLang = this._lang.review;
    this._missionService.scrollTop(true);
    this.placeHolder();
    this.route.params.subscribe(params => {
      this.cityAddress = params['city'];
      this.catName = params['name'];
      this.catId = params['id'];
      this.providerId = params['Pid'];
      this.reviewList(this.providerId);
    });
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  backClicked() {
    this._location.back();
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = $(window).scrollTop();
    if (number > 100) {
      this.headerrestOpen = true;
    } else {
      this.headerrestOpen = false;
    }
  }


  reviewList(id) {
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      console.log(this.latlng)
    }
    this.providerId = id;
    this.reviewListAll();
    let list = {
      providerId: id,
      categoryId: this.catId,
      lat: this.latlng.lat,
      lng: this.latlng.lng
    }
    this.loaderList = false;
    this._service.providerReviews(list)
      .subscribe((res: any) => {
        this.providerRw = true;
        this.loaderList = true;
        console.log("customer/providerDetails", res);
        this.listReview = res.data;
      }, err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      })

  }

  reviewMore() {
    this.pageno += 1;
    this.reviewListAll();
  }

  allProvider() {
    this.providerRw = false;
  }

  reviewListAll() {
    let list = {
      providerId: this.providerId,
      pageNo: this.pageno
    }
    this._service.listAllReview(list)
      .subscribe((res: any) => {
        console.log("customer/providerReview", res);
        let data = res.data.reviews;
        if (data && data.length == 5) {
          this.loadMore = true;
        } else {
          this.loadMore = false;
        }

        for (var i = 0; i < data.length; i++) {
          this.listReviews.push(data[i]);
        }
        this.timeReview();

      }, err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      })
  }

  timeReview() {
    var len = this.listReviews;
    for (var i = 0; i < len.length; i++) {
      if (this.listReviews) {
        var date = this.listReviews[i].reviewAt;
        // console.log(this.listReviews[i].reviewAt)
        var post = date;
        var poston = Number(post);
        var postdate = new Date(poston * 1000);
        var currentdate = new Date();
        var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
        var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);
        if (1 < diffDays) {
          this.listReviews[i].reviewAts = diffDays + " Days ago";
        } else if (1 <= diffHrs) {
          this.listReviews[i].reviewAts = diffHrs + " Hours ago";
        } else {
          this.listReviews[i].reviewAts = diffMins + " Minutes ago";
        }
      }
    }
  }

  selectProvider(id) {
    var loginFlag = this._conf.getItem("isLogin");
    if (loginFlag == "true") {
      this._router.navigate(["./", this.cityAddress, this.catName, this.catId, this.providerId]);
    } else {
      this._missionService.loginOpen(false);
    }
  }

}
