import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateLoader, TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Configuration } from "../app.constant";


// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
   return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
    imports: [
      HttpClientModule,
      CommonModule,
      TranslateModule.forRoot({
         loader: {
           provide: TranslateLoader,
           useFactory: HttpLoaderFactory,
           deps: [HttpClient],
         },
         isolate: false
       })
    ],
    exports: [
      CommonModule,
      TranslateModule,      
    ]  

})
export class SharedModule {
    
     constructor(private translate: TranslateService, private _conf:Configuration) {
      
        translate.addLangs(["English", "Italian"]);
        translate.setDefaultLang('English');
      
        let browserLang = this._conf.getItem("lang") || translate.getBrowserLang();  
        // let browserLang = translate.getBrowserLang();      
         translate.use(browserLang.match(/English|Italian/) ? browserLang : 'English');         
      
        //  this.translate.onLangChange.subscribe((event:LangChangeEvent) => {
        //   console.log('event', event);      
        // })
    }
}
