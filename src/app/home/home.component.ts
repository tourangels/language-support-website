import { Component, OnInit } from "@angular/core";
import { HomeService } from "./home.service";
import { Router } from "@angular/router";
import { NgxCarousel } from "ngx-carousel";
import { MissionService } from "../app.service";
import { ProfileService } from "../profile/profile.service";
import { Configuration } from "../app.constant";
import { LanguageService } from '../app.language';

declare var $: any;
declare var moment: any;
declare var google: any;
declare var flatpickr: any;

@Component({
  selector: "home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  constructor(
    private _service: HomeService,
    private _missionService: MissionService,
    private _router: Router,
    private _conf: Configuration,
    private _serviceProfile: ProfileService,
    private _lang:LanguageService
  ) {
    _missionService.catSearchPopOpen$.subscribe(val => {
      // if (val == true) {
      //   this.getCity();
      // }else{
      this.typeBooking(val);
      // }
    });
  }

  public carouselTileItems: Array<any>;
  public carouselTileRecommend: Array<any>;
  public ratingItems: Array<any>;

  public carouselTile: NgxCarousel;
  public carouselRecommend: NgxCarousel;
  public carouselReview: NgxCarousel;
 
  homeLang:any;
  trendingList: any;
  citys: any;
  dataList: any;
  cityName: any;
  catSearch: any;
  searchCate: string;
  cityId: any;
  loaderList = false;
  loaderLists = false;
  catLoaderList = true;
  shimmer: any = [];
  categoryToggle: any;
  catAll: any;

  bookingType: any;
  catName: any;
  catId: any;
  cityAddress: any;
  locAddress: any;
  ipAddress: any;
  latlng: any;
  ipList: any;

  bookinList: any;
  sendDate: any = [];
  sendTime: any = [];
  asapSchedule = false;
  locationLists = false;
  bookingList = false;
  count = 1;
  bookDate: any;
  bookTitle: any;
  otherDate = false;
  scheduleDateTimeStamp: any;
  scheduleTime: any;
  bookDates: any;

  bInvoice: any;
  bId: any;
  reviewMsg: any;
  ratingList: any = [];
  reviewSuccess: any;
  reviewErr: any;
  popupCart = false;

  defaultImage =
    "https://d2qb2fbt5wuzik.cloudfront.net/iserve_goTasker/images/serviceEmpty.jpg";
  defaultTrending =
    "https://d2qb2fbt5wuzik.cloudfront.net/iserve_goTasker/images/Trbusser.svg";
  offset = 50;

  cLocation: any;
  index: any;

  ngOnInit() {
    flatpickr(".BtypePicker", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      minDate: moment()
        .add(9, "days")
        .format("YYYY-MM-DD"),
      allowInput: false
      // defaultDate:   moment().format('YYYY-MM-DD HH:mm:ss')
      // dateFormat: "H:i",
      // enableTime: true,
      // minTime: "16:00",
      // minuteIncrement:30,
      // dateFormat: "Y-m-d",
    });
    var nexthour = moment()
      .add(1, "hour")
      .format("HH:mm");
    // console.log(nexthour);
    flatpickr(".BtypeTimePicker", {
      // allowInput: true,
      enableTime: true,
      noCalendar: true,
      dateFormat: "h:i K",
      altFormat: "h:i K",
      defaultDate: nexthour,
      minTime: nexthour,
      minuteIncrement: 15
    });
    this.homeLang = this._lang.home;
    this._missionService.scrollTop(false);
    this._conf.removeItem("backClicked");
    let urlPath = this._conf.getItem("pathname");
    if (urlPath && urlPath != "/") {
      // let list = urlPath.split("/");
      // var index = list.indexOf("website");
      // if (index > -1) {
      //   list.splice(index, 1);
      // }
      // console.log(list.join('/'));
      // let url = list.join('/');
      this._router.navigate([urlPath]);
      this._conf.removeItem("pathname");
    } else {
      this.cityName = this._conf.getItem("city") || "Select city";
      this.placeHolder();
      this.getIp();

      this.getDate();
      this.getTimes();
      this.reviewPending();
    }
    this.loaderList = false;
    setTimeout(() => {
      this.getCity();
      this.loaderList = true;
    }, 1000);

    // setTimeout(() => {
    //   if (this.catLoaderList) {
    //     this._missionService.scrollTops(false);
    //   }
    // }, 5000);
    this.selectLocate();

    this.carouselTile = {
      grid: { xs: 2.5, sm: 2.5, md: 2.5, lg: 2.5, all: 0 },
      slide: 1,
      speed: 400,
      // animation: 'lazy',
      point: {
        visible: false
      },
      load: 1,
      touch: true
      // easing: 'ease'
    };

    this.carouselRecommend = {
      grid: { xs: 2, sm: 3, md: 3, lg: 4, all: 0 },
      slide: 1,
      speed: 400,
      // animation: 'lazy',
      point: {
        visible: false
      },
      load: 1,
      touch: true
      // easing: 'ease'
    };

    this.carouselReview = {
      grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
      slide: 1,
      speed: 400,
      // animation: 'lazy',
      point: {
        visible: false
      },
      load: 1,
      touch: true
      // easing: 'ease'
    };
  }

  public carouselTileLoad(evt: any) {
    const len = this.carouselTileItems.length;
    for (let i = len; i < len; i++) {
      this.carouselTileItems.push(i);
    }
  }

  public carouselTileReview(evt: any) {
    const len = this.ratingItems.length;
    for (let i = len; i < len; i++) {
      this.ratingItems.push(i);
    }
  }

  public carouselRecommendLoad(evt: any) {
    const len = this.carouselTileRecommend.length;
    for (let i = len; i < len; i++) {
      this.carouselTileRecommend.push(i);
    }
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  selectLocate() {
    let no = this._conf.getItem("notaryTask");
    let latlng = this._conf.getItem("latlng");
    let cityName = this._conf.getItem("city");
    if (!latlng && no != "1" && !cityName) {
      this.cLocation = true;
      $("#notaryTask").modal("show");
    }
  }

  submitLocate() {
    var loc = $("#locatFill").val();
    this.cityName = this._conf.getItem("city");
    // console.log(this.index, this.cityId);
    if (this.index >= 0) {
      this.listCat(this.index);
      $(".modal").modal("hide");
    } else {
      if (loc) {
        this.cLocation = false;
        this.getCity();
        $(".modal").modal("hide");
        this._conf.setItem("place", JSON.stringify(loc));
      } else {
        this.locAddress = "";
      }
    }
  }

  getIp() {
    this._service.getIpAddress().subscribe((res: any) => {
      // let ipList = res.loc.split(",");
      this.ipList = {
        lat: res.latitude,
        lng: res.longitude
      };
      this.ipAddress = res.ip;
      this.getCity();
      /** lat lng get ip**/
      this._conf.setItem("ip", res.ip);
      this.cityName = this._conf.getItem("city") || res.city;
      let latlng = this._conf.getItem("latlng");
      if (!latlng) {
        this._conf.setItem("latlng", JSON.stringify(this.ipList));
      }
    });
  }

  reviewPending() {
    console.log("customer/reviewAndRatingPending");
    var loginFlag = this._conf.getItem("isLogin");
    if (loginFlag == "true") {
      this._service.pendingReviewList().subscribe(
        (res: any) => {
          console.log("customer/reviewAndRatingPending", res);
          let rating = res.data;
          console.log("rating[0]", rating[0]);
          if (rating && rating.length > 0) {
            this.bId = rating[0];
            this.getReviewList();
          }
        },
        err => {
          console.log(err.error.message);
        }
      );
    }
  }

  getReviewList() {
    let list = {
      bId: this.bId
    };
    this._serviceProfile.getReview(list).subscribe(
      (res: any) => {
        console.log("booking/invoice", res);
        this.bInvoice = res.data;
        if (this.bInvoice.customerRating) {
          this.bInvoice.customerRating.forEach(x => {
            x.rating = 5;
          });
        }
        if (
          this.bInvoice.additionalService &&
          this.bInvoice.additionalService.length > 0
        ) {
          this.bInvoice.additionalService.forEach(x => {
            x.price = parseFloat(x.price).toFixed(2);
          });
        }
        this.bId = res.data.bookingId;
        res.data.bookingRequestedFor = moment
          .unix(res.data.bookingRequestedFor)
          .format("DD MMM, YYYY | hh:mm A");
        res.data.accounting.total = parseFloat(
          res.data.accounting.total
        ).toFixed(2);
        this.reviewSuccess = false;
        $("#reviewProvider").modal("show");
        this.popupCart = false;
      },
      err => {
        console.log(err.error.message);
      }
    );
  }

  show_rating_txt(val, list, i) {
    // console.log(this.bInvoice.customerRating[i])
    this.bInvoice.customerRating[i].rating = val;
    // this.reviewErr = false;
    // let ratingList = {
    //   _id: list._id,
    //   name: list.name,
    //   rating: val
    // }
    // let itemIndex = this.ratingList.findIndex(x => x._id == list._id);
    // if (itemIndex > -1) {
    //   this.ratingList[itemIndex].rating = val;
    // } else {
    //   this.ratingList.push(ratingList);
    // }
    // console.log(this.ratingList)
  }

  reviewSubmitList() {
    this.ratingList = [];
    if (this.bInvoice.customerRating) {
      this.bInvoice.customerRating.forEach(x => {
        let ratingList = {
          _id: x._id,
          name: x.name,
          rating: x.rating
        };
        this.ratingList.push(ratingList);
      });
    }

    if (this.ratingList && this.ratingList.length > 0) {
      let list = {
        bookingId: this.bId,
        rating: this.ratingList,
        review: this.reviewMsg
      };
      this.loaderLists = true;
      this._serviceProfile.reviewSubmit(list).subscribe((res: any) => {
        console.log("booking/invoice", res);
        this.loaderLists = true;
        this.reviewSuccess = true;
        setTimeout(() => {
          $(".modal").modal("hide");
          this._router.navigate([""]);
        }, 2500);
      });
    } else {
      this.reviewErr = "Please rate";
    }
  }

  getTimes() {
    let hoursRequired = 46;
    for (let i = 0; i <= hoursRequired; i++) {
      var hour_array = {
        hour: "",
        nexthour: ""
      };
      hour_array.hour = moment()
        .add(i, "hour")
        .format("h A");
      var curr = moment()
        .add(i, "hour")
        .format("h h");
      var current_hour = i;
      current_hour++;
      hour_array.nexthour = moment()
        .add(current_hour, "hour")
        .format("h A");
      this.sendTime.push(hour_array);
    }
  }

  getCity() {
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
    } else {
      this.latlng = {
        lat: parseFloat(this.ipList.lat),
        lng: parseFloat(this.ipList.lng)
      };
    }
    this._service.cityList().subscribe(
      (res: any) => {
        console.log("website/city", res);
        res.data.sort((a, b) => {
          var nameA = a.city.toLowerCase(),
            nameB = b.city.toLowerCase();
          if (nameA < nameB)
            //sort string ascending
            return -1;
          if (nameA > nameB) return 1;
          return 0; //default return value (no sorting)
        });
        this.citys = res.data;
        this.dataList = res.data;
        // this.cityName = this.citys[0].city;
        this.categoriesList("0");
        // this.ratList("0");
        //this.citys[0].id
      },
      err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      }
    );
  }

  filterSearch(val) {
    var city = this.dataList.filter(x => {
      if (val.length > 0) {
        // console.log(val.toString().toLowerCase());
        return (
          x.city
            .toString()
            .toLowerCase()
            .indexOf(val.toString().toLowerCase()) === 0
        );
      } else {
        return this.citys;
      }
    });
    console.log(city);
    this.citys = city;
  }

  listCat(i) {
    this.index = i;
    $("#cityName").html(this.citys[i].city);
    this.cityName = this.citys[i].city;
    this.cityId = this.citys[i].id;
    this.categoriesList(this.citys[i].id);
    // this.ratList(this.citys[i].id);
    this._missionService.catSearch(this.citys[i].id);
  }

  categoriesList(val) {
    let list = {
      lat: this.latlng.lat,
      long: this.latlng.lng,
      ipAddress: this.ipAddress,
      cityId: val || "0"
    };
    this.catLoaderList = true;
    this._service.catList(list, "").subscribe(
      (res: any) => {
        this.catLoaderList = false;
        console.log("website/categories", res);
        this.carouselTileItems = res.data.catArr;
        this.carouselTileRecommend = res.data.recommendedArr;
        this.trendingList = res.data.trendingArr;
      },
      err => {
        console.log(err._body);
        console.log(err.error.message);
        this.catLoaderList = false;
        this.carouselTileItems = [];
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      }
    );
  }

  ratList(val) {
    let list = {
      lat: this.latlng.lat,
      long: this.latlng.lng,
      ipAddress: this.ipAddress,
      cityId: val || "0"
    };
    this._service.ratingList(list).subscribe(
      (res: any) => {
        console.log("website/rating", res);
        this.ratingItems = res.data;
      },
      err => {
        console.log(err._body);
      }
    );
  }
  // index = 0;
  searchCat(val) {
    if (val.length > 0) {
      let list = {
        lat: this.latlng.lat,
        long: this.latlng.lng,
        ipAddress: this.ipAddress,
        cityId: this.cityId || "0"
      };
      this._service.catList(list, val).subscribe(
        (res: any) => {
          console.log(res);
          this.catSearch = res.data.catArr;
        },
        err => {
          console.log(err._body);
        }
      );

      // $(document).keydown((evt) => {
      //   if (evt.keyCode == 40) { // down arrow
      //     // evt.preventDefault(); // prevents the usual scrolling behaviour
      //     console.log("down arrow")
      //     this.index += 1;
      //     console.log(this.index)
      //   } else if (evt.keyCode == 38) { // up arrow
      //     // evt.preventDefault();
      //     this.index -= 1;
      //     console.log("up arrow")
      //   }
      // });
    } else {
      this.catSearch = false;
    }
  }

  blurSearch() {
    setTimeout(() => {
      this.catSearch = false;
      this.searchCate = "";
    }, 500);
  }

  listCategoryAll(list) {
    this.catAll = list;
    // console.log(list)
    this.listCategory();
  }

  listCategory() {
    $("body").toggleClass("hideHidden");
    this.categoryToggle = !this.categoryToggle;
  }

  addHour(i: number) {
    this.count = Math.min(Math.max(1, this.count + i));
  }

  getDate() {
    let daysRequired = 8;
    for (let i = 1; i <= daysRequired; i++) {
      // console.log(this.send_date);
      var date_array = {
        date: "",
        day: "",
        active: false,
        unformat: ""
      };
      if (i == 1) {
        date_array.day = "Tommorrow";
      } else if (i == 0) {
        date_array.day = "Today";
      } else {
        date_array.day = moment()
          .add(i, "days")
          .format("dddd");
      }
      date_array.date = moment()
        .add(i, "days")
        .format("D");

      date_array.unformat = moment()
        .add(i, "days")
        .format("YYYY-MM-DD HH:mm:ss");

      this.sendDate.push(date_array);
      this.sendDate[0].active = true;
      let date = this.sendDate[0].unformat;
      this.bookDate = moment(date).format("dddd,D MMM Y");
    }
    // var asapNow = moment().format('YYYY-MM-DD HH:mm:ss');
    // this._conf.setItem("bookingDate", asapNow);
    // console.log(this.sendDate);
  }

  selectDate(i) {
    // console.log(i)
    if (i == "Ds") {
      this.otherDate = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.otherDate = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[i].active = true;
      // this._conf.setItem("bookingDate", this.sendDate[i].unformat);
      let date = this.sendDate[i].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
    }
  }

  asapSchedules(val) {
    this.otherDate = false;
    if (val == 0) {
      this.asapSchedule = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.asapSchedule = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[0].active = true;
      // console.log(this.sendDate[0].unformat);
      let date = this.sendDate[0].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
      // this._conf.setItem("bookingDate", this.sendDate[0].unformat);
    }
  }

  dateChange() {
    if (this.asapSchedule == true) {
      this.bookingType = 2;
      this.bookTitle = "Schedule";
      if (this.otherDate == true) {
        let date =
          $(".BtypePicker").val() ||
          moment()
            .add(9, "days")
            .format("YYYY-MM-DD");
        this.bookDate = moment(date).format("dddd,D MMM Y");
      } else {
        this.bookDate = this.bookDates;
      }
    } else {
      this.bookingType = 1;
      let bookDate = new Date();
      this.bookTitle = "Book Now";
      this.bookDate = moment()
        .add(bookDate, "days")
        .format("dddd,D MMM Y");
    }

    let time =
      $(".BtypeTimePicker").val() ||
      moment()
        .add(1, "hour")
        .format("hh:mm:ss A");
    this.scheduleDateTimeStamp = moment(
      this.bookDate + " " + time,
      "dddd,D MMM Y hh:mm:ss A"
    ).unix();
    this.scheduleTime = this.count * 60;

    let list = {
      bookingType: this.bookingType,
      bookDate: this.bookDate,
      bookTime: time,
      bookCount: this.count,
      scheduleDateTimeStamp: this.scheduleDateTimeStamp,
      scheduleTime: this.scheduleTime
    };
    this._conf.setItem("bookingDates", JSON.stringify(list));
    // console.log(this.bookDate, this.scheduleDateTimeStamp, this.scheduleTime);
    this.notaryService();
  }

  sType: any;
  typeBooking(list) {
    this.cLocation = false;
    var loginFlag = this._conf.getItem("isLogin");
    if ((list.billing_model == 3 || list.billing_model == 4) && !loginFlag) {
      this._missionService.loginOpen(false);
    } else {
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.catName = list.cat_name;
      this.catId = list._id;
      this.bookingList = list;
      this.locationLists = false;
      this.sType = list.service_type;
      this.cityAddress = this._conf.getItem("city");
      $("#notaryTask").modal("show");
      if (this._conf.getItem("place")) {
        $("#locatFill").val(JSON.parse(this._conf.getItem("place")));
      }
    }
  }

  trendingService(list) {
    this.cityAddress = list.city;
    // this.catName = list.categoryName;
    this.catId = list.categoryId;
    this.carouselTileItems.forEach(x => {
      var index = x.category.findIndex(y => y._id == this.catId);
      // console.log(x.category[index]);
      this.typeBooking(x.category[index]);
    });
    // this.bookingList = list;
    // this.locationLists = false;
    // // this.notaryService();
    // $("#locatFill").val(JSON.parse(this._conf.getItem("place")));
    // $("#notaryTask").modal("show");
  }

  notaryService() {
    // var catName = this.catName.split(" ").join("_");
    var catName = this.catName.replace(/[^A-Z0-9]/gi, "_");
    // this._router.navigate(["./", this.cityAddress, catName, this.catId]);
    if (this.sType == 1) {
      this._router.navigate(["./", this.cityAddress, catName, this.catId, "0"]);
    } else {
      this._router.navigate(["./", this.cityAddress, catName, this.catId]);
    }
  }

  bookingListSchedule() {
    var locat = $("#locatFill").val();
    if (locat) {
      this.locationLists = true;
      this.locAddress = locat;
      this.asapSchedule = false;
      this._conf.setItem("place", JSON.stringify(locat));
    } else {
      this.locAddress = "";
    }
  }

  closeLocation() {
    $("#locatFill").val("");
    this.locAddress = "";
  }

  currentLocation() {
    $(".errorText").hide();
    $(".locateLoader").show();
    setTimeout(() => {
      $(".locateLoader").hide();
      var loc = $("#locatFill").val();
      if (!loc) {
        this.bookingListSchedule();
      }
    }, 3000);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        let placelatlng = {
          lat: lat,
          lng: lng
        };
        console.log("currentLatLng", placelatlng);
        this._conf.setItem("latlng", JSON.stringify(placelatlng));
        var latlng = new google.maps.LatLng(lat, lng);
        var geocoder = (geocoder = new google.maps.Geocoder());
        geocoder.geocode({ latLng: latlng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              for (
                var i = 0;
                i < results[1].address_components.length;
                i += 1
              ) {
                var addressObj = results[1].address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                  if (addressObj.types[j] === "locality") {
                    var City = addressObj.long_name;
                    // console.log(addressObj.long_name);
                    this.cityAddress = City;
                  }
                  if (addressObj.types[j] === "administrative_area_level_1") {
                    var state = addressObj.short_name;
                    // console.log(addressObj.short_name);
                  }
                }
              }
              $("#locatFill").val(results[0].formatted_address);
              this.locAddress = results[0].formatted_address;
              this.cityAddress = City;
              this._conf.setItem("city", City);
              this.cityName = City;
              $(".locateLoader").hide();
            }
          }
        });
      }, this.geoError);
    } else {
      console.log("Geolocation is not supported by this browser.");
    }
  }

  geoError() {
    console.log("Geolocation is not supported by this browser.");
  }

  locationSearch() {
    $(".errorText").hide();
    var input = document.getElementById("locatFill");
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener("place_changed", () => {
      var place = autocomplete.getPlace();
      let placelatlng = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
      };
      this._conf.setItem("latlng", JSON.stringify(placelatlng));
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === "locality") {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
            this.cityAddress = City;
          }
          if (addressObj.types[j] === "administrative_area_level_1") {
            var state = addressObj.short_name;
          }
        }
      }
      this.cityName = City;
      this._conf.setItem("city", City);
    });
  }
}
