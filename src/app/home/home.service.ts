import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Configuration } from '../app.constant';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  constructor(private _http: HttpClient, private _conf: Configuration) {
    // this.headers.append('lan', '1');
  }

  getIpAddress() {
    return this._http.get('https://ipapi.co/json/');
  }

  cityList() {
    // console.log("test", this.headers)
    return this._http.get(this._conf.WebsiteUrl + 'city', {
      headers: this._conf.headers
    });
    // console.log(this._conf.headers)
    // return this._http.get(this._conf.WebsiteUrl + 'city', { headers: this._conf.headers })
    //   .map(res => res.json())
    //   .map((res) => {
    //     console.log("website/city", res);
    //     return res;
    //   })
  }

  pendingReviewList() {
    return this._http.get(this._conf.CustomerUrl + 'reviewAndRatingPending', {
      headers: this._conf.headers
    });
  }

  catList(list, search) {
    console.log(list);
    // console.log(this._conf.headers)
    if (search) {
      var Url =
        this._conf.WebsiteUrl +
        'categories/' +
        list.lat +
        '/' +
        list.long +
        '/' +
        list.ipAddress +
        '/' +
        list.cityId +
        '/' +
        search;
    } else {
      var Url =
        this._conf.WebsiteUrl +
        'categories/' +
        list.lat +
        '/' +
        list.long +
        '/' +
        list.ipAddress +
        '/' +
        list.cityId +
        '/0';
    }
    return this._http.get(Url, { headers: this._conf.headers });
  }

  ratingList(list) {
    console.log(list);
    var Url =
      this._conf.WebsiteUrl +
      'rating/' +
      list.lat +
      '/' +
      list.long +
      '/' +
      list.ipAddress +
      '/' +
      list.cityId;
    return this._http.get(Url, { headers: this._conf.headers });
  }

  aboutList() {
    var Url = this._conf.WebsiteUrl + 'aboutUs';
    return this._http.get(Url, { headers: this._conf.headers });
  }

  contactList() {
    var Url = this._conf.WebsiteUrl + 'contactUs';
    return this._http.get(Url, { headers: this._conf.headers });
  }
}
