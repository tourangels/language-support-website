import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedLazyModule } from '../shared/shared-lazy.module';
@NgModule({
  imports: [
    CommonModule,
    SharedLazyModule
  ],
  declarations: []
})
export class HomeModule { }
