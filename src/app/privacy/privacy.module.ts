import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PrivacyComponent } from './privacy.component';
import { SharedLazyModule } from '../shared/shared-lazy.module';

const routes: Routes = [  
  { path: '', component: PrivacyComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedLazyModule 
  ],
  declarations: [
    PrivacyComponent
  ]
})
export class PrivacyModule { }
