import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Configuration } from '../app.constant';



@Injectable({
    providedIn: 'root'
})

export class HeaderService {


    constructor(private _http: HttpClient, private _conf: Configuration) {
        // this.headers.append('lan', '1');
    }

    getIpAddress() {
        return this._http.get('https://ipapi.co/json/');
    }

    loginGuest(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'guestSignIn', body, { headers: this._conf.headers });
    }

    login(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'signIn', body, { headers: this._conf.headers });
    }

    getProfiles() {
        this._conf.setAuth();
        let url = this._conf.CustomerUrl + 'profile/me';
        return this._http.get(url, { headers: this._conf.headers });
      }

    signUp(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'registerUser', body, { headers: this._conf.headers });
    }

    signUpOtp(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'verifyPhoneNumber', body, { headers: this._conf.headers });
    }


    forGot(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'forgotPassword', body, { headers: this._conf.headers });
    }

    resendOtp(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'resendOtp', body, { headers: this._conf.headers });
    }

    OtpForgot(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'verifyVerificationCode', body, { headers: this._conf.headers });
    }

    checkEmail(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'emailValidation', body, { headers: this._conf.headers });
    }

    checkPhone(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'phoneNumberValidation', body, { headers: this._conf.headers });
    }

    checkreferralCode(list) {
        let body = list;
        console.log(body);
        return this._http.post(this._conf.CustomerUrl + 'referralCodeValidation', body, { headers: this._conf.headers });
    }

    resetPassword(list) {

        let body = list;
        console.log(body);
        return this._http.patch(this._conf.CustomerUrl + 'password', body, { headers: this._conf.headers });
    }

    logout() {
        // var token = this._conf.getItem("sessionToken");
        // this._conf.headers.set('authorization', token);
        return this._http.post(this._conf.CustomerUrl + 'logout', { headers: this._conf.headers });
    }




}
