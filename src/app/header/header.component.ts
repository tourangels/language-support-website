import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageService } from '../app.language';
import { HeaderService } from './header.service';
import { ProfileService } from '../profile/profile.service';
import { MissionService } from './../app.service';
import { Configuration } from '../app.constant';
import { AuthService } from 'angular2-social-login';
import { Location } from '@angular/common';

declare var $: any;
declare var google: any;
declare var moment: any;
declare var FB: any;

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(
    private _lang: LanguageService,
    private _zone: NgZone,
    private _auth: AuthService,
    private _conf: Configuration,
    private _service: HeaderService,
    private _serviceProfile: ProfileService,
    private _router: Router,
    private _missionService: MissionService,
    private _location: Location
  ) {
    _missionService.loginOpen$.subscribe(val => {
      console.log('val', val);
      if (val == true) {
        $('#expirySession').modal('show');
      } else {
        this.loginSignup();
        this.popOverlay = true;
      }
    });
    _missionService.scrollTop$.subscribe(val => {
      $('.modal').modal('hide');
      $('html, body').animate({ scrollTop: '0px' }, 500);
    });

    _missionService.scrollTops$.subscribe(val => {
      // if (val == false) {
      //   this.getLocation();
      // } else {
      $('html, body').animate({ scrollTop: '500px' }, 500);
      // }
    });

    _missionService.addWallets$.subscribe(val => {
      this.getProfile();
    });
  }

  headerLang: any;
  popOverlay: any;

  focusMobile = false;
  loginSignToggle = false;
  menuToggle = false;
  loaderList = false;
  emailPhone = false;
  loginToggle = false;
  otpToggle = false;
  signupToggle = false;
  oneTime = false;
  rePwd = false;
  signupoTp = false;
  phoneNumber: string;
  password: string;
  OtpNumber = '1111';
  newPassword: string;
  rePassword: string;
  CPassword: string;
  fullName: string;
  lastName: string;
  email: string;
  sId: string;
  emailOrPhone: string;
  referralCode: string;
  email_Error: any;
  phone_Error: any;
  pwd_Error: any;
  registerSave = false;
  errMsg: any;
  termsVisible = false;
  sub: any;
  timer = false;
  loginFlag = false;
  profileName: any;
  profilePic: any;
  placelatlng: any;
  ipAddress: any;

  walletMoney: any;

  latlng: any;
  infowindow: any;

  ngOnInit() {
    this.getIp();
    this.currentLocation();
    this.headerLang = this._lang.header;
    var loginFlag = this._conf.getItem('isLogin');
    if (loginFlag == 'true') {
      this.loginFlag = true;
      this.profileName = this._conf.getItem('firstName');
      this.getProfile();
    } else {
      this.loginFlag = false;
    }

    // setTimeout(() => {
    //   if (typeof this.placelatlng == "undefined") {
    //     this.getLocation();
    //   }
    // }, 5000);

    // $.get("https://ipapi.co/json/", (response) => {
    //   this._conf.setItem("ip", response.ip);
    // }, "jsonp");

    $('#regisCouFlag, #regisCouLogin, #regisCouOtpLogin').intlTelInput({
      autoHideDialCode: true,
      geoIpLookup: callback => {
        $.get('https://ipapi.co/json/', () => { }).always(resp => {
          var countryCode = resp && resp.country ? resp.country : '';
          callback(countryCode);
        });
      },
      initialCountry: 'auto',
      nationalMode: true,
      separateDialCode: true
      // utilsScript: "https://d2qb2fbt5wuzik.cloudfront.net/ideliver_amgular/js/utils.js"
    });
  }

  getLocation() {
    $('.modal').modal('hide');
    $('#cLocation').modal('show');
    var latlng;
    if (typeof this.latlng == 'undefined') {
      latlng = this.placelatlng;
    } else if (typeof this.placelatlng == 'undefined') {
      latlng = this.latlng;
    } else {
      latlng = {
        lat: 12.9833,
        lng: 77.5833
      };
    }
    let ip = this._conf.getItem('ip');
    if (!ip) {
      // console.log("gLoip", latlng)
      this._conf.setItem('ip', '106.51.66.44');
      this.ipAddress = '106.51.66.44';
      this.guestLogIn();
    }
    console.log('gLo', latlng);
    this.continueMapLocation(latlng);
  }

  getIp() {
    this._service.getIpAddress().subscribe((res: any) => {
      console.log('ip', res);
      this._conf.setItem('ip', res.ip);
      this._conf.setItem('city', res.city);
      // let ipList = res.loc.split(',');
      this.latlng = {
        lat: res.latitude,
        lng: res.longitude
      };
      // this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
      this.ipAddress = this._conf.getItem('ip');
      this.guestLogIn();
    });
  }

  countdown() {
    this.timer = true;
    var timer2 = '2:00';
    var interval = setInterval(() => {
      var timer = timer2.split(':');
      //by parsing integer, I avoid all extra string processing
      var minutes = parseInt(timer[0], 10);
      var seconds = parseInt(timer[1], 10);
      --seconds;
      minutes = seconds < 0 ? --minutes : minutes;
      seconds = seconds < 0 ? 59 : seconds;
      seconds = seconds < 10 ? 0 + seconds : seconds;
      //minutes = (minutes < 10) ?  minutes : minutes;
      // console.log("minutes", minutes, seconds);
      if (seconds == 0) {
        this.timer = false;
      } else {
        this.timer = true;
      }
      $('#countdown2').html(minutes + ':' + seconds);
      if (minutes < 0) clearInterval(interval);
      //check if both minutes and seconds are 0
      if (seconds <= 0 && minutes <= 0) clearInterval(interval);
      timer2 = minutes + ':' + seconds;
    }, 1000);
  }

  currentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        this.placelatlng = {
          lat: lat,
          lng: lng
        };
        console.log('currentLatLng', this.placelatlng);
        this._conf.setItem('latlng', JSON.stringify(this.placelatlng));
        var latlng = new google.maps.LatLng(lat, lng);
        var geocoder = (geocoder = new google.maps.Geocoder());
        geocoder.geocode({ latLng: latlng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              for (
                var i = 0;
                i < results[1].address_components.length;
                i += 1
              ) {
                var addressObj = results[1].address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                  if (addressObj.types[j] === 'locality') {
                    var city = addressObj.long_name;
                  }
                  if (addressObj.types[j] === 'country') {
                    var country = addressObj.short_name;
                  }
                }
              }
              this._conf.setItem(
                'place',
                JSON.stringify(results[0].formatted_address)
              );
              this._conf.setItem('city', city);
              $('#cityName').html(city);
              $('#locatFill').val(results[0].formatted_address);
              $('#txtPlaced').val(results[0].formatted_address);
              $("#notaryTask").modal("hide");
              $('.arealats').val(lat);
              $('.arealngs').val(lng);
              this.continueMapLocation(this.placelatlng);
            }
          }
        });
      }, this.geoError);
    } else {
      console.log('Geolocation is not supported by this browser.');
    }
  }

  geoError() {
    console.log('Geolocation is not supported by this browser.');
  }

  guestLogIn() {
    var token = this._conf.getItem('sessionToken');
    if (!token) {
      let datetime = moment().format('YYYY-MM-DD HH:mm:ss');
      let list = {
        deviceId: datetime,
        appVersion: '10',
        devMake: '1',
        devModel: '1',
        devType: 3,
        deviceTime: datetime,
        deviceOsVersion: '1',
        ipAddress: this.ipAddress
      };
      this._service.loginGuest(list).subscribe(
        (res: any) => {
          console.log('customer/guestSignIn', res);
          this._conf.setItem('sid', res.data.sid);
          this._conf.setItem('sessionToken', res.data.token);
        },
        err => {
          console.log(err._body);
          if (err._body) {
            var error = JSON.parse(err._body);
            // this.errMsg = error.message;
          }
          // console.log(JSON.parse(err._body));
        }
      );
    }
  }

  sideMenuList() {
    $('body').toggleClass('hideHidden');
    this.menuToggle = !this.menuToggle;
  }

  loginSignupOpen() {
    this.sideMenuList();
    this.loginSignup();
  }

  profilePage() {
    this._router.navigate(['./profile']);
  }

  becomeList() {
    $('body').removeClass('hideHidden');
    this.menuToggle = false;
    this._router.navigate(['./becomeatasker']);
  }

  listDetails(url, i) {
    $('body').removeClass('hideHidden');
    this.menuToggle = false;
    // console.log(url)
    if (i != 0) {
      this._conf.setItem('pathname', url);
      this._conf.setItem('acc', i);
      this._router.navigate(['./**']);
    } else {
      this._router.navigate([url]);
    }
  }

  loginSignup() {
    this.login();
    $('body').toggleClass('hideHidden');
    this.loginSignToggle = !this.loginSignToggle;
    let latlngs = this._conf.getItem('latlng');
    console.log('latlngLo', this.placelatlng, latlngs);
    if (!this.placelatlng && latlngs) {
      let latlng = JSON.parse(this._conf.getItem('latlng'));
      this.placelatlng = {
        lat: latlng.lat,
        lng: latlng.lng
      };
    }
  }

  overLayLogin() {
    var loginFlag = this._conf.getItem('isLogin');
    if (loginFlag == 'true' && this.popOverlay) {
      this._location.back();
      this.loginSignup();
    } else {
      this.loginSignup();
    }
  }

  signUp() {
    this.loginToggle = true;
    this.otpToggle = false;
    this.signupToggle = true;
    this.oneTime = false;
    this.rePwd = false;
  }
  forgot() {
    this.loginToggle = true;
    this.otpToggle = true;
    this.signupToggle = false;
    this.oneTime = false;
    this.rePwd = false;
  }
  login() {
    this.emailPhone = false;
    this.resetValues();
    this.loginToggle = false;
    this.otpToggle = false;
    this.signupToggle = false;
    this.oneTime = false;
    this.rePwd = false;
    this.signupoTp = false;
    this.errMsg = false;
  }

  phoneEmailValidation() {
    this.emailValidation(this.email, 1);
    this.mobileValidation(this.phoneNumber, 1);
  }

  emailValidation(value, i) {
    // console.log(value);
    var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

    if (regexEmail.test(value)) {
      this.email_Error = '0';
      this.registerSave = true;
      this.errMsg = false;
      if (i == 1) {
        this.emailCheck(value);
      }
    } else {
      this.email_Error = '1';
      if (value.length > 5) {
        this.errMsg = 'The email ID is invalid';
      }
      this.registerSave = false;
      // setTimeout(() => {
      //   this.errMsg = false;
      // }, 3000)
    }
  }

  mobileValidation(value, i) {
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;

    // if (value.match(regexPhone)) {
    //   this.phone_Error = '0';
    //   this.registerSave = true;
    //   this.errMsg = false;
    //   if (i == 1) {
    //     this.phoneCheck(value);
    //   }
    // } else {
    //   var val = value.replace(/([^+0-9]+)/gi, '');
    //   this.phoneNumber = val;
    //   this.phone_Error = '1';
    //   if (value.length > 5) {
    //     this.errMsg = 'The Mobile number entered is incorrect';
    //   }
    //   this.registerSave = false;      
    // }
    this.registerSave = true;
    var val = value.replace(/([^+0-9]+)/gi, '');
    this.phoneNumber = val;
    if (i == 1) {
      this.phoneCheck(value);
    }
  }

  emailCheck(value) {
    let list = {
      email: value.trim().toLowerCase()
    };
    this._service.checkEmail(list).subscribe(
      (res: any) => {
        this.email_Error = '0';
        // this.errMsg = false;
        this.registerSave = true;
      },
      err => {
        console.log(err.error.message);
        this.errMsg = err.error.message;
        this.email_Error = '1';
      }
    );
  }

  phoneCheck(value) {
    let flag = $('#regisCouFlag').intlTelInput('getSelectedCountryData');
    let list = {
      phone: value,
      countryCode: '+' + flag.dialCode
    };
    this._service.checkPhone(list).subscribe(
      (res: any) => {
        this.phone_Error = '0';
        // this.errMsg = false;
        this.registerSave = true;
      },
      err => {
        console.log(err.error.message);
        this.errMsg = err.error.message;
        this.phone_Error = '1';
      }
    );
  }

  passwordValidation(val) {
    if (val.length > 6) {
      this.pwd_Error = '0';
      this.errMsg = false;
      this.registerSave = true;
    } else {
      this.pwd_Error = '1';
      this.errMsg = 'The password must be atleast 7 characters.';
      this.registerSave = false;
      // setTimeout(() => {
      //   this.errMsg = false;
      // }, 3000)
    }
  }

  referalValidation() {
    this.errMsg = false;
    if (this.referralCode.length > 0) {
      let list = {
        referralCode: this.referralCode
      };
      this._service.checkreferralCode(list).subscribe(
        (res: any) => {
          console.log('referralCodeValidation', res);
          this.registration();
        },
        err => {
          console.log(err.error.message);
          var error = err.error;
          this.errMsg = error.message;
        }
      );
    } else {
      // console.log('ref')
      this.registration();
    }
  }

  resetValues() {
    this.fullName = '';
    this.lastName = '';
    this.phoneNumber = '';
    this.email = '';
    this.password = '';
    this.newPassword = '';
    this.CPassword = '';
    this.OtpNumber = '';
    this.referralCode = '';
    this.email_Error = 1;
  }

  emailPhones(val) {
    this.emailPhone = val == 0 ? false : true;
  }

  visibleTerm() {
    this.phoneEmailValidation();
    this.termsVisible = !this.termsVisible;
  }

  progressBar() {
    this.errMsg = false;
    var progress = setInterval(() => {
      var $bar = $('.bar');
      if ($bar.width() >= 400) {
        clearInterval(progress);
      } else {
        $bar.width($bar.width() + 5 + '%');
      }
    }, 100);
  }

  logIn() {
    this.loaderList = true;
    this.progressBar();
    let flag = $('#regisCouLogin').intlTelInput('getSelectedCountryData');
    let datetime = moment().format('YYYY-MM-DD HH:mm:ss');
    if (this.emailPhone == true) {
      this.emailOrPhone = this.phoneNumber;
    } else {
      this.emailOrPhone = this.email;
    }
    // if (this._conf.getItem('latlng')) {
    //   var latlng = JSON.parse(this._conf.getItem('latlng'));
    // }
    // console.log(Number(this.placelatlng.lat));

    let list = {
      emailOrPhone: this.emailOrPhone.trim().toLowerCase(),
      // countryCode: "+" + flag.dialCode,
      password: this.password,
      deviceId: datetime,
      pushToken: '',
      appVersion: '10',
      devMake: '1',
      devModel: '1',
      devType: 3,
      deviceTime: datetime,
      loginType: 1,
      deviceOsVersion: '1',
      facebookId: '',
      googleId: '',
      latitude: this.placelatlng.lat,
      longitude: this.placelatlng.lng
    };
    setTimeout(() => {
      this.loaderList = false;
      this.errMsg = false;
    }, 5000);
    this._service.login(list).subscribe(
      (res: any) => {
        this.loaderList = false;
        console.log(res);
        // if (res.message == "Login Successfully.") {
        // this.errMsg = res.message;
        this._conf.setItem('isLogin', 'true');
        this._conf.setItem('PublishableKey', res.data.PublishableKey);
        this._conf.setItem('countryCode', res.data.countryCode);
        this._conf.setItem('currencyCode', res.data.currencyCode);
        this._conf.setItem('email', res.data.email);
        this._conf.setItem('fcmTopic', res.data.fcmTopic);
        this._conf.setItem('firstName', res.data.firstName);
        this._conf.setItem('phone', res.data.phone);
        this._conf.setItem('referralCode', res.data.referralCode);
        this._conf.setItem('sid', res.data.sid);
        this._conf.setItem('sessionToken', res.data.token);
        this.loginFlag = true;
        this.profileName = this._conf.getItem('firstName');
        this.getProfile();
        // setTimeout(() => {
        this.loginSignup();
        var currentLocation = window.location;
        this._conf.setItem('pathname', currentLocation.pathname);
        this._router.navigate(['']);
        // }, 500);
        // } else {
        //   this.errMsg = res.message;
        // }
      },
      err => {
        this.loaderList = false;
        // console.log(err.error.message);
        var error = err.error;
        this.errMsg = error.message;
        // console.log(JSON.parse(err._body));
      }
    );
  }

  registration() {
    this.loaderList = true;
    this.progressBar();
    let flag = $('#regisCouFlag').intlTelInput('getSelectedCountryData');
    let datetime = moment().format('YYYY-MM-DD HH:mm:ss');
    // if (this._conf.getItem('latlng')) {
    //   var latlng = JSON.parse(this._conf.getItem('latlng'));
    // }
    let list = {
      firstName: this.fullName,
      lastName: this.lastName,
      email: this.email.trim().toLowerCase(),
      password: this.password,
      countryCode: '+' + flag.dialCode,
      phone: this.phoneNumber,
      dateOfBirth: datetime,
      profilePic: '',
      loginType: 1,
      facebookId: '',
      googleId: '',
      latitude: this.placelatlng.lat,
      longitude: this.placelatlng.lng,
      preferredGenres: '',
      termsAndCond: 0,
      devType: 3,
      deviceId: datetime,
      pushToken: '',
      appVersion: '10',
      devMake: '1',
      devModel: '1',
      deviceTime: datetime,
      deviceOsVersion: '1',
      referralCode: this.referralCode,
      ipAddress: this.ipAddress
    };
    if (this.termsVisible == true) {
      list.termsAndCond = 1;
    }
    var googleId = $('.googleId').val();
    var fbId = $('.fbId').val();
    if (fbId) {
      list.googleId = '';
      list.facebookId = fbId;
      list.loginType = 2;
    }
    if (googleId) {
      list.facebookId = '';
      list.googleId = googleId;
      list.loginType = 3;
    }
    setTimeout(() => {
      this.loaderList = false;
      this.errMsg = false;
    }, 3000);
    this._service.signUp(list).subscribe(
      (res: any) => {
        this.loaderList = false;
        // if (res.message == "Got The Details.") {
        this.sId = res.data.sid;
        this.signupoTp = true;
        this.countdown();
        // } else {
        //   this.errMsg = res.message;
        // }
      },
      err => {
        this.loaderList = false;
        console.log(err._body);
        var error = err.error;
        this.errMsg = error.message;
      }
    );
  }

  resendOtp() {
    this.loaderList = true;
    this.progressBar();
    this.countdown();
    let list = {
      userId: this.sId,
      trigger: 1,
      userType: 1
    };
    setTimeout(() => {
      this.loaderList = false;
      this.errMsg = false;
    }, 3000);
    this._service.resendOtp(list).subscribe(
      (res: any) => {
        this.loaderList = false;
        this.errMsg = res.message;
      },
      err => {
        this.loaderList = false;
        var error = err.error;
        this.errMsg = error.message;
      }
    );
  }

  registrationOtp() {
    this.loaderList = true;
    this.progressBar();
    let list = {
      code: this.OtpNumber,
      userId: this.sId
    };
    setTimeout(() => {
      this.loaderList = false;
      this.errMsg = false;
    }, 3000);
    this._service.signUpOtp(list).subscribe(
      (res: any) => {
        this.loaderList = false;
        // if (res.message == "Phone Number has been verified.") {
        this.errMsg = res.message;
        this._conf.setItem('isLogin', 'true');
        this._conf.setItem('PublishableKey', res.data.PublishableKey);
        this._conf.setItem('countryCode', res.data.countryCode);
        this._conf.setItem('currencyCode', res.data.currencyCode);
        this._conf.setItem('email', res.data.email);
        this._conf.setItem('fcmTopic', res.data.fcmTopic);
        this._conf.setItem('firstName', res.data.firstName);
        this._conf.setItem('phone', res.data.phone);
        this._conf.setItem('referralCode', res.data.referralCode);
        this._conf.setItem('sid', res.data.sid);
        this._conf.setItem('sessionToken', res.data.token);
        this.loginFlag = true;
        this.getProfile();
        setTimeout(() => {
          this.loginSignup();
          var currentLocation = window.location;
          this._conf.setItem('pathname', currentLocation.pathname);
          this._router.navigate(['']);
        }, 1000);
        // } else {
        //   this.errMsg = res.message;
        // }
      },
      err => {
        this.loaderList = false;
        var error = err.error;
        this.errMsg = error.message;
      }
    );
  }

  forgotPassword() {
    this.loaderList = true;
    this.progressBar();
    let flag = $('#regisCouOtpLogin').intlTelInput('getSelectedCountryData');
    if (this.emailPhone == true) {
      this.emailOrPhone = this.phoneNumber;
      var type = 1;
    } else {
      this.emailOrPhone = this.email;
      var type = 2;
    }
    let list = {
      emailOrPhone: this.emailOrPhone.trim().toLowerCase(),
      countryCode: '+' + flag.dialCode,
      userType: 1,
      type: type
    };
    setTimeout(() => {
      this.loaderList = false;
      this.errMsg = false;
    }, 5000);
    this._service.forGot(list).subscribe(
      (res: any) => {
        this.loaderList = false;
        if (
          res.message ==
          'Verification Code has been sent to your registered Mobile Number / Email' &&
          type == 2
        ) {
          this.errMsg = res.message;
          setTimeout(() => {
            this.login();
            this.errMsg = false;
          }, 500);
          // this.oneTime = true;
          // this.otpId = res._id;
        } else if (
          res.message ==
          'Verification Code has been sent to your registered Mobile Number / Email' &&
          type == 1
        ) {
          this.sId = res.data.sid;
          this.oneTime = true;
        } else {
          this.errMsg = res.message;
        }
      },
      err => {
        this.loaderList = false;
        var error = err.error;
        this.errMsg = error.message;
      }
    );
  }

  fogotOtp() {
    this.loaderList = true;
    this.progressBar();
    let list = {
      code: this.OtpNumber,
      userId: this.sId,
      trigger: 2,
      userType: 1
    };
    setTimeout(() => {
      this.loaderList = false;
      this.errMsg = false;
    }, 3000);
    this._service.OtpForgot(list).subscribe(
      (res: any) => {
        this.loaderList = false;
        if (res.message == 'Verification sucess') {
          this.rePwd = true;
        } else {
          this.errMsg = res.message;
        }
      },
      err => {
        this.loaderList = false;
        var error = err.error;
        this.errMsg = error.message;
      }
    );
  }

  reSetPwd() {
    setTimeout(() => {
      this.loaderList = false;
      this.errMsg = false;
    }, 3000);
    if (this.newPassword == this.CPassword) {
      this.loaderList = true;
      this.progressBar();
      let list = {
        password: this.CPassword,
        userId: this.sId,
        userType: 1
      };
      this._service.resetPassword(list).subscribe(
        (res: any) => {
          this.loaderList = false;
          if (res.message == 'Password updated successfully') {
            this.errMsg = res.message;
            setTimeout(() => {
              this.login();
            }, 500);
          } else {
            this.errMsg = res.message;
          }
        },
        err => {
          this.loaderList = false;
          var error = err.error;
          this.errMsg = error.message;
        }
      );
    } else {
      this.errMsg = 'password not match';
    }
  }

  getProfile() {
    this.getWallet();
    this._service.getProfiles().subscribe(
      (res: any) => {
        console.log('customer/profile', res);
        this.profileName = res.data.firstName;
        this.profilePic = res.data.profilePic;
      },
      err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      }
    );
  }

  getWallet() {
    this._serviceProfile.getwalletMoney().subscribe(
      (res: any) => {
        console.log('customer/wallet', res);
        this.walletMoney = res.data;
      },
      err => {
        console.log(err.error.message);
      }
    );
  }

  onFacebookLoginClick() {
    // console.log("test")
    $('.spinFl').show();
    setTimeout(() => {
      $('.spinFl').hide();
    }, 3000);
    this.sub = this._auth.login('facebook').subscribe(result => {
      console.log(result);
      this.me();
    });
  }

  me() {
    FB.api(
      '/me?fields=id,picture,name,first_name,email,gender',
      (result: any) => {
        let datetime = moment().format('YYYY-MM-DD HH:mm:ss');
        let list = {
          emailOrPhone: result.email,
          // countryCode: "+" + flag.dialCode,
          password: '',
          deviceId: datetime,
          pushToken: '',
          appVersion: '10',
          devMake: '1',
          devModel: '1',
          devType: 3,
          deviceTime: datetime,
          loginType: 2,
          deviceOsVersion: '1',
          facebookId: result.id,
          googleId: '',
          latitude: this.placelatlng.lat,
          longitude: this.placelatlng.lng
        };

        this._service.login(list).subscribe(
          (res: any) => {
            console.log(res);
            // if (res.message == "Login Successfully.") {
            this.errMsg = res.message;
            this._conf.setItem('isLogin', 'true');
            this._conf.setItem('PublishableKey', res.data.PublishableKey);
            this._conf.setItem('countryCode', res.data.countryCode);
            this._conf.setItem('currencyCode', res.data.currencyCode);
            this._conf.setItem('email', res.data.email);
            this._conf.setItem('fcmTopic', res.data.fcmTopic);
            this._conf.setItem('firstName', res.data.firstName);
            this._conf.setItem('phone', res.data.phone);
            this._conf.setItem('referralCode', res.data.referralCode);
            this._conf.setItem('sid', res.data.sid);
            this._conf.setItem('sessionToken', res.data.token);
            $('.spinFl').hide();
            this.loginFlag = true;
            this.getProfile();
            setTimeout(() => {
              this.loginSignup();
              var currentLocation = window.location;
              this._conf.setItem('pathname', currentLocation.pathname);
              this._router.navigate(['']);
            }, 1000);
            // } else {
            //   this.errMsg = res.message;
            // }
          },
          err => {
            console.log(err._body);
            var error = err.error;
            this.errMsg = error.message;
            // console.log(JSON.parse(err._body));
            if (err.status == 404) {
              this.signUp();
              this.onFacebookSignupClick();
            }
          }
        );
      }
    );
  }

  onGoogleLoginClick() {
    $('.spinGl').show();
    setTimeout(() => {
      $('.spinGl').hide();
      this.errMsg = false;
    }, 5000);
    this.sub = this._auth.login('google').subscribe(googleUser => {
      console.log(googleUser);
      this.onSuccess(googleUser);
    });
  }

  onSuccess = googleUser => {
    this._zone.run(() => {
      let datetime = moment().format('YYYY-MM-DD HH:mm:ss');
      // if (this._conf.getItem('latlng')) {
      //   var latlng = JSON.parse(this._conf.getItem('latlng'));
      // }
      let list = {
        emailOrPhone: googleUser.email,
        // countryCode: "+" + flag.dialCode,
        password: '',
        deviceId: datetime,
        pushToken: '',
        appVersion: '10',
        devMake: '1',
        devModel: '1',
        devType: 3,
        deviceTime: datetime,
        loginType: 3,
        deviceOsVersion: '1',
        facebookId: '',
        googleId: googleUser.uid,
        latitude: this.placelatlng.lat,
        longitude: this.placelatlng.lng
      };
      this._service.login(list).subscribe(
        (res: any) => {
          // if (res.message == "Got The Details.") {
          // this.sId = res.data.sid;
          // this.signupoTp = true;
          // this.countdown();
          // } else {
          //   this.errMsg = res.message;
          // }
          this._conf.setItem('isLogin', 'true');
          this._conf.setItem('PublishableKey', res.data.PublishableKey);
          this._conf.setItem('countryCode', res.data.countryCode);
          this._conf.setItem('currencyCode', res.data.currencyCode);
          this._conf.setItem('email', res.data.email);
          this._conf.setItem('fcmTopic', res.data.fcmTopic);
          this._conf.setItem('firstName', res.data.firstName);
          this._conf.setItem('phone', res.data.phone);
          this._conf.setItem('referralCode', res.data.referralCode);
          this._conf.setItem('sid', res.data.sid);
          this._conf.setItem('sessionToken', res.data.token);
          $('.spinGl').hide();
          this.loginFlag = true;
          this.getProfile();
          setTimeout(() => {
            this.loginSignup();
            var currentLocation = window.location;
            this._conf.setItem('pathname', currentLocation.pathname);
            this._router.navigate(['']);
          }, 1000);
        },
        err => {
          this.loaderList = false;
          var error = err.error;
          this.errMsg = error.message;
          if (err.status == 404) {
            this.signUp();
            this.onGoogleSignupClick();
          }
        }
      );
    });
  };

  onFacebookSignupClick() {
    // console.log("test")
    $('.spinFs').show();
    setTimeout(() => {
      $('.spinFs').hide();
      this.errMsg = false;
    }, 5000);
    this.sub = this._auth.login('facebook').subscribe(result => {
      console.log(result);
      this.fbSignup();
    });
  }

  fbSignup() {
    FB.api(
      '/me?fields=id,picture,name,first_name,email,gender',
      (result: any) => {
        let datetime = moment().format('YYYY-MM-DD HH:mm:ss');

        let name = result.name.split(' ');
        this.fullName = name[0];
        this.lastName = name[1];
        this.email = result.email;
        this.email_Error = '0';
        $('.fbId').val(result.id);
        $('.googleId').val('');
        $('.spinFs').hide();
        // let list = {
        //   firstName: result.name,
        //   lastName: "",
        //   email: result.email,
        //   password: "",
        //   countryCode: "",
        //   phone: "",
        //   dateOfBirth: datetime,
        //   profilePic: "",
        //   loginType: 2,
        //   facebookId: result.id,
        //   googleId: "",
        //   latitude: this.placelatlng.lat,
        //   longitude: this.placelatlng.lng,
        //   preferredGenres: "",
        //   termsAndCond: 0,
        //   devType: 3,
        //   deviceId: "12345",
        //   pushToken: "",
        //   appVersion: "10",
        //   devMake: "1",
        //   devModel: "1",
        //   deviceTime: datetime,
        //   deviceOsVersion: "1",
        //   referralCode: "",
        //   ipAddress: ""
        // }

        // this._service.signUp(list)
        //   .subscribe((res:any) => {
        //     console.log(res)
        //     // if (res.message == "Login Successfully.") {
        //     this.errMsg = res.message;
        //     this._conf.setItem("isLogin", "true");
        //     this._conf.setItem("PublishableKey", res.data.PublishableKey);
        //     this._conf.setItem("countryCode", res.data.countryCode);
        //     this._conf.setItem("currencyCode", res.data.currencyCode);
        //     this._conf.setItem("email", res.data.email);
        //     this._conf.setItem("fcmTopic", res.data.fcmTopic);
        //     this._conf.setItem("firstName", res.data.firstName);
        //     this._conf.setItem("phone", res.data.phone);
        //     this._conf.setItem("referralCode", res.data.referralCode);
        //     this._conf.setItem("sid", res.data.sid);
        //     this._conf.setItem("sessionToken", res.data.token);
        //     setTimeout(() => {
        //       this.loginSignup();
        //     }, 500);
        //     // } else {
        //     //   this.errMsg = res.message;
        //     // }
        //   }, (err) => {
        //     console.log(err._body);
        //     if (err._body) {
        //       var error = JSON.parse(err._body);
        //       this.errMsg = error.message;
        //     }
        //     // console.log(JSON.parse(err._body));
        //   });
      }
    );
  }

  onGoogleSignupClick() {
    $('.spinGs').show();
    setTimeout(() => {
      $('.spinGs').hide();
      this.errMsg = false;
    }, 5000);
    this.sub = this._auth.login('google').subscribe(googleUser => {
      console.log(googleUser);
      this.onSuccessSignup(googleUser);
    });
  }

  onSuccessSignup = googleUser => {
    this._zone.run(() => {
      let datetime = moment().format('YYYY-MM-DD HH:mm:ss');
      // if (this._conf.getItem('latlng')) {
      //   var latlng = JSON.parse(this._conf.getItem('latlng'));
      // }

      let name = googleUser.name.split(' ');
      this.fullName = name[0];
      this.lastName = name[1];
      this.email = googleUser.email;
      this.email_Error = '0';
      $('.googleId').val(googleUser.uid);
      $('.fbId').val('');
      $('.spinGs').hide();
      // let list = {
      //   firstName: googleUser.name,
      //   lastName: "",
      //   email: googleUser.email,
      //   password: "",
      //   countryCode: "",
      //   phone: "",
      //   dateOfBirth: datetime,
      //   profilePic: "",
      //   loginType: 3,
      //   facebookId: "",
      //   googleId: googleUser.uid,
      //   latitude: this.placelatlng.lat,
      //   longitude: this.placelatlng.lng,
      //   preferredGenres: "",
      //   termsAndCond: 0,
      //   devType: 3,
      //   deviceId: "12345",
      //   pushToken: "",
      //   appVersion: "10",
      //   devMake: "1",
      //   devModel: "1",
      //   deviceTime: datetime,
      //   deviceOsVersion: "1",
      //   referralCode: "",
      //   ipAddress: ""
      // }
      // this._service.signUp(list)
      //   .subscribe((res:any) => {

      //     setTimeout(() => {
      //       this.loginSignup();
      //     }, 500);
      //   }, (err) => {
      //     this.loaderList = false;
      //     console.log(err._body);
      //     if (err._body) {
      //       var error = JSON.parse(err._body);
      //       this.errMsg = error.message;
      //     }
      //   });
    });
  };

  logOut() {
    this._service.logout().subscribe(
      (res: any) => {
        this.clearData();
      },
      err => {
        this.clearData();
        // this._conf.clear();
        // this.loginFlag = false;
        // this._router.navigate(['']);
        console.log(err);
        // if (err._body) {
        //   var error = JSON.parse(err._body);
        //   this.errMsg = error.message;
        // }
      }
    );
    console.log();
  }

  clearData() {
    this._conf.clear();
    this.loginFlag = false;
    this._router.navigate(['']);
    this._conf.setItem('notaryTask', 1);
  }

  closeLocations() {
    this.locAddressed = '';
  }

  locAddressed: any;
  submitLocate() {
    var locat = $('#txtPlaced').val();
    var lat = $('.arealats').val();
    var lng = $('.arealngs').val();
    if (locat && lat) {
      let latlng = {
        lat: lat,
        lng: lng
      };
      this._conf.setItem('latlng', JSON.stringify(latlng));
      this._conf.setItem('place', JSON.stringify(locat));
      $('.modal').modal('hide');
      // this._router.navigate([""]);
      this._missionService.catSearchPop(true);
      this._router.navigate(['']);
    } else {
      this.locAddressed = '';
    }
  }

  changeLocations() {
    var input = document.getElementById('txtPlaced');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', () => {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
          }
          if (addressObj.types[j] === 'administrative_area_level_1') {
            var state = addressObj.short_name;
            // console.log(addressObj.short_name);
          }
        }
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      this.latlng = {
        lat: lat,
        lng: lng
      };
      this.locAddressed = $('#txtPlaced').val();
      $('.areaNames').val($('#txtPlaced').val());
      $('.arealats').val(lat);
      $('.arealngs').val(lng);
    });
  }

  locationChanges() {
    setTimeout(() => {
      this.continueMapLocation(this.latlng);
    }, 500);
  }

  continueMapLocation(latlng) {
    var latlng = latlng;
    var map = new google.maps.Map(document.getElementById('mapDatas'), {
      center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
      zoom: 15,
      disableDefaultUI: true,
      mapTypeControl: true
    });

    setTimeout(() => {
      google.maps.event.trigger(map, 'resize');
      map.setCenter(latlng);
      var marker = new google.maps.Marker({
        map: map
      });
    }, 300);

    this.infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: true
    });

    var latlng = new google.maps.LatLng(latlng.lat, latlng.lng);
    var geocoder = (geocoder = new google.maps.Geocoder());
    geocoder.geocode({ latLng: latlng }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          // $(".addressArea").val(results[0].formatted_address);
          // $(".areaName").val(results[0].formatted_address);
          // $(".arealat").val(latlng.lat);
          // $(".arealng").val(latlng.lng);
        }
      }
    });

    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, 'dragend', () => {
      geocoder.geocode({ latLng: marker.getPosition() }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            this.locAddressed = results[0].formatted_address;
            $('#txtPlaced').val(results[0].formatted_address);
            $('.areaNames').val(results[0].formatted_address);
            $('.arealats').val(marker.getPosition().lat());
            $('.arealngs').val(marker.getPosition().lng());
            this.infowindow.setContent(results[0].formatted_address);
            this.infowindow.open(map, marker);
          }
        }
      });
    });
  }
}
