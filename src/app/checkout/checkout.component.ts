import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewpriceService } from '../viewprice/viewprice.service';
import { LanguageService } from '../app.language';
import { MissionService } from '../app.service';
import { Configuration } from '../app.constant';
import { CheckoutService } from './checkout.service';
import { Location } from '@angular/common';
import { ProfileService } from '../profile/profile.service';

declare var $: any;
declare var google: any;

declare var require: any;
var creditCardType = require('credit-card-type');
var getTypeInfo = require('credit-card-type').getTypeInfo;
var CardType = require('credit-card-type').types;
declare var moment: any;
declare var Stripe: any;
declare var flatpickr: any;
declare var toastr: any;

@Component({
  selector: 'checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  constructor(
    private _lang: LanguageService,
    private route: ActivatedRoute,
    private _router: Router,
    private _serviceView: ViewpriceService,
    private _conf: Configuration,
    private _zone: NgZone,
    private _location: Location,
    private _missionService: MissionService,
    private _service: CheckoutService,
    private _serviceProfile: ProfileService
  ) { }

  catId: any;
  catName: any;
  serviceList: any;
  cartList: any;
  action = 1;
  cartListActive: any;

  shimmer: any = [];
  loaderList = false;

  checkLang: any;
  newAddress: any;
  changeNewAddress: any;
  checkoutBtn: any;
  place: any;
  latlng: any;

  searchLocationToggle: any;
  typeOne = false;
  typeTwo = false;
  typeThree = false;
  otherText = false;
  addressType = 3;
  Others: string;
  selectedAdd: any;

  customerStripeId: any;
  payGetCartList: any;
  monthYear: string;
  Month: string;
  Year: string;
  cardCredit: string;
  cardBrand: string;
  cardcvv: string;
  cardHolder: string;
  validDM = false;
  validCD: any;
  stripeToken: any;
  stripeCardDetails = false;
  errorSaveCart: any;
  PaymentLoader = false;
  promoCode: any;
  grandTotal: any;
  orderSuccess: any;
  newCardToggle: any;
  serviceType: any;
  serviceTypehr: any;
  billing_model: any;
  providerId: any;
  providerList: any;
  ratingTrue: any;
  savedAddress: any;
  addressMap:string;


  bookingType = 1;
  timing: any;
  bookinList: any;
  sendDate: any = [];
  sendTime: any = [];
  asapSchedule = false;
  locationLists = false;
  bookingList = false;
  count = 1;
  bookDate: any;
  bookTitle: any;
  otherDate = false;
  scheduleDateTimeStamp: any;
  scheduleTime: any;
  bookDates: any;
  addressID: any;
  errorMsg: any;
  cartCount: any;
  CouponToggle: any;
  couponCode: string;
  jobDescription: string;
  successMsg: any;
  errMsg: any;
  loaderApply: any;
  couponPrice: any;
  deliveryFee: any;
  promoId: any;
  cancelBtn: any;

  cardWallet: any;
  cardWallets: any;
  index: any;
  id: any;
  cardId: any;
  addMoney: string;
  walletMoney: any;
  paymentMethod = 1;
  paidByWallet = 0;
  walletSubmit = false;


  infowindow: any;

  ngOnInit() {
    flatpickr('.BtypePicker', {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: 'Y-m-d',
      minDate: moment().add(9, 'days').format('YYYY-MM-DD'),
      allowInput: false
    });
    var nexthour = moment().add(1, 'hour').format('HH:mm');
    flatpickr('.BtypeTimePicker', {
      // allowInput: true,
      enableTime: true,
      noCalendar: true,
      dateFormat: "h:i K",
      altFormat: 'h:i K',
      minDate: nexthour,
      minuteIncrement: 15,
    });

    this._missionService.scrollTop(true);
    this.placeHolder();
    this.checkLang = this._lang.checkout;
    this.route.params.subscribe(params => {
      this.catId = params['id'];
      this.providerId = params['Pid'];
      this.listService(this.catId);
      this.getCartList(this.catId);
    });
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      this.newAddress = JSON.parse(this._conf.getItem("place"));
      $(".arealat").val(this.latlng.lat);
      $(".arealng").val(this.latlng.lng);
    }

    // let bookDate = this._conf.getItem("bookingDate");
    // this.bookDate = moment().add(bookDate, 'days').format('dddd,D MMM Y');
    // var hour = moment().add(0, 'hour').format('h A');
    // var nexthour = moment().add(1, 'hour').format('h A');
    // this.timing = hour + " - " + nexthour;
    let stripKey = this._conf.getItem("PublishableKey");
    Stripe.setPublishableKey(stripKey);

    this.sendGetCart();
    this.sendGetAddress();
    this.getWallet();
    this.getDate();
    this.getTimes();

    if (this._conf.getItem('bookingDates')) {
      var date = JSON.parse(this._conf.getItem('bookingDates'));
      console.log("date", date);
      this.bookTitle = date.bookingType == "1" ? "Book Now" : "Schedule";
      this.bookDate = date.bookDate
      this.bookingType = date.bookingType;
      this.scheduleDateTimeStamp = date.scheduleDateTimeStamp;
      this.scheduleTime = date.scheduleTime;
      if (this.bookingType == 2) {
        this.timing = date.bookTime;
      } else {
        this.timing = moment(new Date()).format('hh:mm:ss');
      }
    } else {
      this.dateChange();
    }
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
    setTimeout(() => {
      this.loaderList = true;
    }, 1000)

  }

  arrow = 3;
  arrowChange(i: number) {
    this.arrow = Math.min(3, Math.max(1, this.arrow + i));
    // console.log(this.arrow)
  }

  listService(id) {
    let list = {
      catId: id,
      providerId: this.providerId
    }
    // this.loaderList = false;
    this._serviceView.viewServiceList(list)
      .subscribe((res: any) => {
        // this.loaderList = true;
        console.log("customer/services", res);
        this.serviceList = res;
        this.providerList = res.providerData;
        this.catName = res.categoryData.cat_name;
        this.serviceType = res.categoryData.service_type;
        this.billing_model = res.categoryData.billing_model;
        // for (var i = 0; i < this.serviceList.length; i++) {
        //   this.serviceList[i].service.forEach(x => {
        //     x.btnActive = true;
        //   });
        // }
        // this.getCartList(this.catId);
      }, err => {
        var error = err.error;
        console.log(error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }      
      });
  }


  getCartList(id) {
    let list = {
      catId: id,
      providerId: this.providerId
    }
    this._serviceView.getCart(list)
      .subscribe((res: any) => {
        console.log("customer/get-cart", res);
        this.cartList = res.data;
        // this.serviceType = res.data.serviceType;
        this.serviceTypehr = res.data.serviceType;
        if (this.billing_model == 3 || this.billing_model == 4) {
          this._conf.setItem("backClicked", '1');
        }
        this.grandTotal = parseFloat(res.data.totalAmount).toFixed(2);
        var cartActive = false;
        this.cartList.item.forEach(x => {
          if (x.quntity > 0) {
            // console.log("sfa", this.cartList.item.length)
            this.cartCount = this.cartList.item.length;
            cartActive = true;
          }
        });

        if (cartActive) {
          this.cartListActive = true;
        } else {
          this.cartListActive = false;
          this.backClicked(1);
        }
      }, err => {
        // console.log(err.error);
        setTimeout(() => {
          this.backClicked(1);
        }, 1000);
      });
  }

  backClicked(link) {
    let city = this.serviceList.categoryData.city;
    var catName = this.catName.replace(/[^A-Z0-9]/ig, "_");
    if (link == 1) {
      this._router.navigate([city, catName, this.catId]);
    } else {
      this._router.navigate([city, catName, this.catId, this.providerId]);
    }
    // this._location.back();
  }

  reviewList(id) {
    let city = this.serviceList.categoryData.city;
    var catName = this.catName.replace(/[^A-Z0-9]/ig, "_");
    this._router.navigate(["./review", city, catName, this.catId, id]);
  }

  addMinusCart(id, action, index) {
    var maxQty = this.cartList.item[index].maxquantity;
    var Qty = this.cartList.item[index].quntity;
    if ((action == 1) && (maxQty > Qty) || (action == 2) && (maxQty >= Qty) || (action == 0) || this.cartList.serviceType == 2) {
      this.action = action;
      this.addCartList(id);
      this.cartList.item[index].loader = true;
      setTimeout(() => {
        if (action == 1) {
          this.cartList.item[index].quntity += 1;
        } else {
          this.cartList.item[index].quntity -= 1;
        }
        this.cartList.item[index].loader = false;
      }, 500);
    } else {
      this.cartList.item[index].maxqty = true;
      setTimeout(() => {
        this.cartList.item[index].maxqty = false;
      }, 1500);
    }
  }

  addCart(id, i, index) {
    this.action = 1;
    this.addCartList(id);
    this.serviceList[i].service[index].loader = true;
    setTimeout(() => {
      this.serviceList[i].service[index].loader = false;
    }, 500);

  }

  addCartList(id) {
    let list = {
      serviceType: this.serviceTypehr,
      bookingType: this.bookingType,
      categoryId: this.catId,
      providerId: this.providerId,
      serviceId: id,
      quntity: 1,
      action: this.action
    }
    if (this.action == 0) {
      list.action = 2;
    }
    this._serviceView.addCart(list)
      .subscribe((res: any) => {
        console.log("customer/cart", res);
        // this.cartList = res.data;
        this.getCartList(this.catId);
      }, err => {
        var error = err.error;
        console.log(error.message);
      });
  }


  pickUpChange(val) {
    this.newAddress = val;
    this.changeNewAddress = true;
  }

  paymentNavMenu(id: number) {
    $(".list_card").removeClass("active");
    $('#' + id + 'paynavMenu').addClass("active");
    $(".list_card_information").removeClass("active");
    $('#' + id + 'payNavMenu').addClass("active");
    // this.walletSubmit = false;
  }

  paymentNavMenus(id: number) {
    $(".allSmallCheckout").removeClass("active");
    $('#' + id + id + 'payNavMenu').addClass("active");
    this.paymentMethod = id == 1 ? 1 : 2;
    if (id == 4) {
      this.cardWallets = true;
    } else {
      this.cardWallets = false;
    }
    this.checkoutBtn = true;
    // this.walletSubmit = false;
  }

  cardToggle(val) {
    $("body").toggleClass("hideHidden");
    this.newCardToggle = !this.newCardToggle;
    if (val == 1) {
      this.cardWallet = true;
    } else {
      if (this.cardId) {
        this.cardWallet = false;
      } else {
        this.errorSaveCart = "Please add a card";
        this.cardWallet = true;
      }
    }

  }

  editAddress(index) {
    // console.log(this.savedAddress[index])
    $("body").toggleClass("hideHidden");
    this.searchLocationToggle = !this.searchLocationToggle;
    // this.closeOpenSearch();
    this.addressID = this.savedAddress[index]._id;
    let address1 = this.savedAddress[index].addLine1;
    var latAddress = this.savedAddress[index].latitude;
    var lngAddress = this.savedAddress[index].longitude;
    this.selectType(this.savedAddress[index].taggedType, this.savedAddress[index].taggedAs);
    this.latlng = {
      lat: latAddress,
      lng: lngAddress
    }
    // $("#txtPlaces").val(address1);
    this.addressMap = address1;
    $(".areaName").val(address1);
    $(".arealat").val(this.latlng.lat);
    $(".arealng").val(this.latlng.lng);
    this.continueMap();
  }

  selectType(type: number, val) {
    this.addressType = type;
    if (type == 1) {
      this.typeOne = !this.typeOne;
      this.typeTwo = false;
      this.typeThree = false;
    } else if (type == 2) {
      this.typeTwo = !this.typeTwo;
      this.typeOne = false;
      this.typeThree = false;
    } else if (type == 3) {
      this.Others = val;
      this.otherText = true;
      this.typeThree = !this.typeThree;
      this.typeTwo = false;
      this.typeOne = false;
    }
  }

  closeOpenSearch() {
    $("body").toggleClass("hideHidden");
    this.searchLocationToggle = !this.searchLocationToggle;

    let latlng = this._conf.getItem("latlng");
    if ((latlng) && $("body").hasClass("hideHidden")) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      var address = JSON.parse(this._conf.getItem("place"));
    }
    // $("#txtPlaces").val(address);
    this.addressMap = address;
    $(".areaName").val(address);
    $(".arealat").val(this.latlng.lat);
    $(".arealng").val(this.latlng.lng);
    this.typeThree = false;
    this.typeTwo = false;
    this.typeOne = false;
    this.otherText = false;
    this.continueMap();
  }

  changeAddr() {
    this.changeNewAddress = false;
  }

  addressSelect(i) {
    this.selectedAdd = this.savedAddress[i];
    this.newAddress = this.savedAddress[i].addLine1;
    this.changeNewAddress = true;
    this.checkoutBtn = false;
  }

  sendGetAddress() {
    this.Others = "";
    this.addressID = "";
    this._serviceProfile.getAddress()
      .subscribe((res: any) => {
        console.log("customer/address", res);
        this.savedAddress = res.data;
        this.savedAddress.forEach(x => {
          x.taggedAs = x.taggedAs.toLowerCase();
          if (x.taggedAs == 'home') {
            x.taggedType = 1;
          } else if (x.taggedAs == 'office' || x.taggedAs == 'work') {
            x.taggedType = 2;
          } else {
            x.taggedType = 3;
          }
        });
      }, (err) => {
        console.log(err.error);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

  saveAddress() {
    this.progressBar();
    this.loaderList = true;
    var address = $(".areaName").val();
    var lat = $(".arealat").val();
    var lng = $(".arealng").val();
    if (this.addressType == 1) {
      var name = "Home";
    } else if (this.addressType == 2) {
      var name = "Office";
    } else if (this.addressType == 3) {
      var name = "Other";
    }
    let list = {
      addLine1: address,
      addLine2: "",
      city: "",
      state: "",
      country: "",
      placeId: "",
      pincode: "",
      latitude: lat,
      longitude: lng,
      taggedAs: name,
      userType: 1
    }
    if (this.Others) {
      list.taggedAs = this.Others;
    }
    if (this.addressID) {
      this._serviceProfile.addressEditSaved(list, this.addressID)
        .subscribe((res: any) => {
          this.addressID = "";
          this.loaderList = true;
          console.log("edit customer/address", res);
          this.sendGetAddress();
          $("body").toggleClass("hideHidden");
          this.searchLocationToggle = false;
        }, (err) => {
          this.loaderList = true;
          console.log(err.error);
          if (err.status == 498) {
            this._missionService.loginOpen(true);
          }
        });
    } else {
      this._serviceProfile.addressSaved(list)
        .subscribe((res: any) => {
          this.loaderList = true;
          console.log("customer/address", res);
          this.sendGetAddress();
          $("body").toggleClass("hideHidden");
          this.searchLocationToggle = false;
        }, (err) => {
          this.loaderList = true;
          console.log(err.error);
          if (err.status == 498) {
            this._missionService.loginOpen(true);
          }
        });
    }
  }

  progressBar() {
    var progress = setInterval(() => {
      var $bar = $('.barStore');
      if ($bar.width() >= 400) {
        clearInterval(progress);
      } else {
        $bar.width($bar.width() + 5 + "%");
      }
    }, 100);
  }


  addHour(i: number) {
    this.count = Math.min(Math.max(1, this.count + i));;
  }


  getDate() {
    let daysRequired = 8;
    for (let i = 1; i <= daysRequired; i++) {
      // console.log(this.send_date);
      var date_array = {
        "date": "",
        "day": "",
        "active": false,
        "unformat": "",
      };
      if (i == 1) {
        date_array.day = "Tommorrow"
      } else if (i == 0) {
        date_array.day = "Today"
      } else {
        date_array.day = moment().add(i, 'days').format('dddd');
      }
      date_array.date = moment().add(i, 'days').format('D');

      date_array.unformat = moment().add(i, 'days').format('YYYY-MM-DD HH:mm:ss');

      this.sendDate.push(date_array);
      this.sendDate[0].active = true;
      let date = this.sendDate[0].unformat;
      this.bookDate = moment(date).format('dddd,D MMM Y');
    }
    // var asapNow = moment().format('YYYY-MM-DD HH:mm:ss');
    // this._conf.setItem("bookingDate", asapNow);
    // console.log(this.sendDate);
  }

  selectDate(i) {
    // console.log(i)
    if (i == "Ds") {
      this.otherDate = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.otherDate = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[i].active = true;
      // this._conf.setItem("bookingDate", this.sendDate[i].unformat);     
      let date = this.sendDate[i].unformat;
      this.bookDates = moment(date).format('dddd,D MMM Y');
    }
  }

  asapSchedules(val) {
    this.otherDate = false;
    if (val == 0) {
      this.asapSchedule = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.asapSchedule = true;
      this.sendDate[0].active = true;
      // console.log(this.sendDate[0].unformat);
      let date = this.sendDate[0].unformat;
      this.bookDates = moment(date).format('dddd,D MMM Y');
      // this._conf.setItem("bookingDate", this.sendDate[0].unformat);
    }
  }

  dateChange() {
    if (this.asapSchedule == true) {
      this.bookingType = 2;
      this.bookTitle = "Schedule";
      if (this.otherDate == true) {
        let date = $(".BtypePicker").val() || moment().add(9, 'days').format('YYYY-MM-DD');
        this.bookDate = moment(date).format('dddd,D MMM Y');
      } else {
        this.bookDate = this.bookDates;
      }
    } else {
      this.bookingType = 1;
      let bookDate = new Date();
      this.bookTitle = "Book Now";
      this.bookDate = moment().add(bookDate, 'days').format('dddd,D MMM Y');
    }
    let time = $(".BtypeTimePicker").val() || moment().add(1, 'hour').format('hh:mm:ss');
    this.scheduleDateTimeStamp = moment(this.bookDate + " " + time, 'dddd,D MMM Y hh:mm:ss').unix();
    this.scheduleTime = this.count * 60;
    if (this.bookingType == 2) {
      this.timing = time;
    } else {
      this.timing = moment(new Date()).format('hh:mm:ss');
    }
    // console.log(this.bookDate, this.timing);
    let list = {
      bookingType: this.bookingType,
      bookDate: this.bookDate,
      bookTime: time,
      bookCount: this.count,
      scheduleDateTimeStamp: this.scheduleDateTimeStamp,
      scheduleTime: this.scheduleDateTimeStamp,
    }
    this._conf.setItem('bookingDates', JSON.stringify(list));
  }

  typeBooking() {
    this.asapSchedule = false;
    this.bookingList = this.serviceList.categoryData;
    console.log(this.bookingList);
    $("#notaryTask").modal("show");
  }

  // function for displaying time
  getTimes() {
    let hoursRequired = 46;
    for (let i = 0; i <= hoursRequired; i++) {
      var hour_array = {
        "hour": "",
        "nexthour": "",
      };
      hour_array.hour = moment().add(i, 'hour').format('h A');
      var curr = moment().add(i, 'hour').format('h h');
      var current_hour = i;
      current_hour++;
      hour_array.nexthour = moment().add(current_hour, 'hour').format('h A');
      this.sendTime.push(hour_array);
    }
  }

  valdationCreditCard(value: any) {
    $(".activeGetCart").removeClass("active");
    this.errorSaveCart = false;
    var visaCards = creditCardType(value);
    var val = value.split(" ").join("");
    if (val.length > 0) {
      val = val.match(new RegExp('.{1,4}', 'g')).join(" ");
    }
    this.cardCredit = val;
    if (value.length <= 17) {
      this.validCD = "The card number should be 15 or 16 digits!";
      // this.stripeCardDetails = true;
    } else {
      this.validCD = false;
      // this.stripeCardDetails = false;
    }

    if (value.length <= 4) {
      if (visaCards != '') {
        // console.log(visaCards[0].type);
        this.cardBrand = visaCards[0].type;
        this.validCD = false;
        this.stripeCardDetails = true;
      } else {
        this.validCD = "invalid card number!";
        this.stripeCardDetails = false;
        this.cardCredit = "";
        console.log("error cardtype");
      }
    }
  }



  valdationDate(val: any) {
    this.errorSaveCart = false;
    this.monthYear = val.replace(/(\d{2}(?!\s))/g, "$1/");

    if (isNaN(val)) {
      val = val.replace(/[^0-9\/]/g, '');
      if (val.split('/').length > 2)
        val = val.replace(/\/+$/, "");
      this.monthYear = val;
    }

    var validDm = /^(0[1-9]|1[0-2])\/\d{4}$/.test(val);
    // console.log(val);
    if (val.length > 2 && validDm == false) {
      this.validDM = true;
      this.stripeCardDetails = false;
    } else {
      this.validDM = false;
      this.stripeCardDetails = true;
    }

    if (val.length == 1) {
      if (val.charAt(0) == 0 || val.charAt(0) == 1) {
        this.monthYear = val;
      } else {
        this.monthYear = '0' + val;
      }
    } else if (val.length == 2) {
      if (val <= 12) {
        this.monthYear = val;
      } else {
        this.monthYear = "";
      }
    }

  }

  valdationcvv(val: any) {
    this.errorSaveCart = false;
    if (val.length == 3) {
      this.stripeCardDetails = true;
    } else {
      this.stripeCardDetails = false;
    }
  }


  stripeCard() {
    // var val = String(Number(this.monthYear));
    var validDateMnth = /^(0[1-9]|1[0-2])\/\d{4}$/.test(this.monthYear);
    // console.log(this.monthYear, validDateMnth);   
    if (this.cardCredit && this.monthYear && this.cardcvv && validDateMnth == true) {
      this.validDM = false;
      this.sendCardStripToken();
    } else {
      this.validDM = true;
      console.log("error stripeCartList");
    }

  }



  sendCardStripToken() {
    this.PaymentLoader = true;
    setTimeout(() => {
      this.PaymentLoader = false;
    }, 5000)
    var MYdetails = this.monthYear.split("/");
    var numberCard = this.cardCredit.split(" ").join("");
    (<any>window).Stripe.card.createToken({
      number: this.cardCredit,
      exp_month: Number(MYdetails[0]),
      exp_year: Number(MYdetails[1]),
      cvc: this.cardcvv
    }, (status: number, response: any) => {
      this._zone.run(() => {
        if (status === 200) {
          this.errorSaveCart = false;
          this.stripeToken = response.id;
          this.sendCardStrip();
          console.log(response.id);
        } else {
          this.errorSaveCart = response.error.message;
          setTimeout(() => {
            this.errorSaveCart = false;
          }, 5000);
          console.log(response.error.message)

        }
      });
    });
  }


  sendCardStrip() {
    var email = this._conf.getItem("email");
    let stripeCartList = {
      cardToken: this.stripeToken,
      email: email
    }
    this._service.paymentStripeCart(stripeCartList)
      .subscribe((res: any) => {
        this.PaymentLoader = false;
        console.log("stripeRes", res);
        $(".modal").modal("hide");
        this.sendGetCart();
        this.resetSaveCart();
        this.cardToggle(1);
      }, (err) => {
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

  sendGetCart() {
    this._service.paymentGetCart()
      .subscribe((res: any) => {
        console.log("paymentget-card", res);
        this.payGetCartList = res.data;
        if (this.payGetCartList && this.payGetCartList.length > 0) {
          this.payGetCartList.forEach(x => {
            x.checked = false;
          });
          this.payGetCartList[0].checked = true;
          this.cardId = this.payGetCartList[0].id;
        }

      }, (err) => {
        console.log("pget", err);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }


  deleteGetCart(i: number, id: any) {
    this.index = i;
    this.id = id;
    $("#cardDelete").modal("show");
  }

  deleteCard() {
    let deletepayGetCart = {
      cardId: this.id
    }
    this._service.paymentGetCartDelete(deletepayGetCart)
      .subscribe((res: any) => {
        this.sendGetCart();
        this.payGetCartList.splice(this.index, 1);
      }, (err) => {
        console.log("pget", err)
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

  selectPayments(i: number) {
    this.payGetCartList.forEach(x => {
      x.checked = false;
    });
    this.payGetCartList[i].checked = true;
    this.cardId = this.payGetCartList[i].id;
  }

  getWallet() {
    this._serviceProfile.getwalletMoney()
      .subscribe((res: any) => {
        console.log("customer/wallet", res);
        this.walletMoney = res.data;
        this._missionService.addWallets(true);
      }, err => {
        console.log(err.error.message)
      });
  }

  addWallet() {
    let list = {
      cardId: this.cardId,
      amount: this.addMoney
    }
    setTimeout(() => {
      this.errorMsg = false;
      this.PaymentLoader = false;
    }, 5000);
    if (this.addMoney) {
      this.PaymentLoader = true;
      this.errorMsg = false;
      this._serviceProfile.getaddwalletMoney(list)
        .subscribe((res: any) => {
          console.log("wallet/recharge", res);
          this.PaymentLoader = false;
          this.errorMsg = res.message;
          $(".modal").modal("hide");         
          this.getWallet();
          this.addMoney = "";
          this.cardToggle(1);
        }, err => {
          this.PaymentLoader = false;
          console.log(err.error.message);
          this.errorMsg = err.error.message;
        });
    } else {
      this.errorMsg = "Please enter the amount to continue."
    }
  }

  CouponToggleList() {
    this.couponCode = "";
    $("body").toggleClass("hideHidden");
    this.CouponToggle = !this.CouponToggle;
  }

  applyCoupons() {
    if (this.couponCode) {
      this.errMsg = false;
      let list = {
        latitude: this.latlng.lat,
        longitude: this.latlng.lng,
        cartId: this.cartList._id,
        paymentMethod: this.paymentMethod,
        paidByWallet:  this.paidByWallet,
        couponCode: this.couponCode,
      }
      setTimeout(() => {
        this.errMsg = false;
        this.loaderApply = false;
      }, 3000);
      this.loaderApply = true;
      this._service.couponList(list)
        .subscribe((res: any) => {
          this.cancelBtn = true;
          this.loaderApply = false;
          this.errMsg = false;
          this.successMsg = res.message;
          this.couponPrice = parseFloat(res.data.discountAmount).toFixed(2);
          this.deliveryFee = res.data.deliveryFee;
          this.promoId = res.data.promoId;
          this.promoCode = this.couponCode;
          this.grandTotal = (parseFloat(this.grandTotal) - parseFloat(this.couponPrice)).toFixed(2);
        }, (err) => {
          this.cancelBtn = true;
          this.loaderApply = false;
          var error = err.error;
          console.log(err.error);
          this.errMsg = error.message;
          if (err.status == 498) {
            this._missionService.loginOpen(true);
          }
        });
    } else {
      this.errMsg = "Enter coupon";
    }
  }

  cancelCoupons() {
    this.couponCode = "";
    this.cancelBtn = false;
    this.couponPrice = "";
    this.grandTotal = parseFloat(this.cartList.totalAmount).toFixed(2)
  }


  resetSaveCart() {
    this.cardCredit = "";
    this.monthYear = "";
    this.cardcvv = "";
    this.cardHolder = "";
  }

  walletPayment(val: number) {
    this.paidByWallet = 1;
    this.paymentMethod = val;
    this.checkoutSubmitCart();
  }

  cardSubmitCart() {
    this.paidByWallet = 0;
    this.paymentMethod = 2;
    this.checkoutSubmitCart();
  }

  cashSubmitCart() {
    this.paidByWallet = 0;
    this.paymentMethod = 1;
    this.checkoutSubmitCart();
  }

  checkoutSubmitCart() {
    this.PaymentLoader = true;
    let datetime = moment().format('YYYY-MM-DD HH:mm:ss');
    let addr = this.selectedAdd;
    let prId = this.providerId == '0' ? '' : this.providerId;
    let list = {
      bookingModel: Number(this.serviceType),
      bookingType: this.bookingType,
      // callType: 2,
      paymentMethod: this.paymentMethod,
      paidByWallet: this.paidByWallet,
      placeName: "",
      addLine1: addr.addLine1,
      addLine2: "",
      city: "",
      state: "",
      country: "",
      placeId: "",
      pincode: "",
      latitude: addr.latitude,
      longitude: addr.longitude,
      providerId: prId,
      categoryId: this.catId,
      cartId: this.cartList._id,
      jobDescription: this.jobDescription || "",
      promoCode: this.promoCode || "",
      paymentCardId: this.paymentMethod == 1 ? '' : this.cardId,
      bookingDate: "",
      scheduleTime: "",
      deviceTime: datetime
    }
    if (this.bookingType == 2) {
      list.bookingDate = String(this.scheduleDateTimeStamp);
      list.scheduleTime = String(this.scheduleTime);
    } else {
      list.bookingDate = "";
    }
    // console.log(list)
    this._service.submitCheckout(list)
      .subscribe((res: any) => {
        $(".modal").modal('hide');
        $("#orderSubmit").modal('show');
        this.PaymentLoader = false;
        this.orderSuccess = res.message;
        toastr["success"]("Yay !We will let you know when the tasker accepts your booking!", "Go-tasker");
      }, err => {
        this.PaymentLoader = false;
        this.errorMsg = err.error.message;
        console.log(err.error.message);
        setTimeout(() => {
          this.errorMsg = false;
        }, 5000);
      });
  }

  profileList() {
    this._router.navigate(["./profile"]);
  }

  deleteAddressList(addressID: any, i: any) {
    $("#deleteAddress").modal("show");
    this.addressID = addressID;
    this.index = i;
  }

  deleteAddress() {
    this._serviceProfile.addressDelete(this.addressID)
      .subscribe((result: any) => {
        this.Others = "";
        this.addressID = "";
        this.savedAddress.splice(this.index, 1);
        $(".modal").modal("hide");
      }, (err) => {
        var error = JSON.parse(err._body);
        // console.log(error)   
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }    
      });
  }

  closeLocation(){
    // $("#txtPlaces").val("");
    this.addressMap = "";
  }


  changeLocation() {
    var input = document.getElementById('txtPlaces');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', () => {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
          }
          if (addressObj.types[j] === 'administrative_area_level_1') {
            var state = addressObj.short_name;
            // console.log(addressObj.short_name);
          }
        }
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      this.latlng = {
        lat: lat,
        lng: lng
      }
      $(".areaName").val($("#txtPlaces").val());
      $(".arealat").val(lat);
      $(".arealng").val(lng);
    });
  }

  locationChange() {
    setTimeout(() => {
      this.continueMap();
    }, 500);
  }


  continueMap() {
    var latlng = this.latlng;
    var map = new google.maps.Map(document.getElementById('mapData'), {
      center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
      zoom: 15,
      disableDefaultUI: true,
      mapTypeControl: true
    });

    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlng);
      var marker = new google.maps.Marker({
        map: map,
      });
    }, 300);

    this.infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: true
    });

    var latlng = new google.maps.LatLng(latlng.lat, latlng.lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          // $(".addressArea").val(results[0].formatted_address);
          // $(".areaName").val(results[0].formatted_address);
          // $(".arealat").val(latlng.lat);
          // $(".arealng").val(latlng.lng);
        }
      }
    });


    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, 'dragend', () => {

      geocoder.geocode({ 'latLng': marker.getPosition() }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            this.addressMap = results[0].formatted_address;
            $(".addressArea").val(results[0].formatted_address);
            $(".areaName").val(results[0].formatted_address);
            $(".arealat").val(marker.getPosition().lat());
            $(".arealng").val(marker.getPosition().lng());
            this.infowindow.setContent(results[0].formatted_address);
            this.infowindow.open(map, marker);
          }
        }
      });
    });
  }

}
