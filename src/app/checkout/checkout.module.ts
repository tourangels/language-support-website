import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CheckoutComponent } from './checkout.component';
import { SharedLazyModule } from '../shared/shared-lazy.module';
const routes: Routes = [  
  { path: '', component: CheckoutComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedLazyModule,
    RouterModule.forChild(routes)    
  ],
  declarations: [
    CheckoutComponent
  ]
})
export class CheckoutModule { }
