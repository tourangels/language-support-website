import { Component, OnInit, HostListener, Inject, NgZone, ViewChild, AfterViewInit } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { MissionService } from '../app.service';
import { Configuration } from '../app.constant';
import { LanguageService } from '../app.language';
import { CheckoutService } from '../checkout/checkout.service';
import { ProfileService } from './profile.service';
import { Paho } from 'ng2-mqtt';
import { CodegenComponentFactoryResolver } from '../../../node_modules/@angular/core/src/linker/component_factory_resolver';
import { createOfflineCompileUrlResolver } from '../../../node_modules/@angular/compiler';


declare var $: any;
declare var google: any;

declare var require: any;
var creditCardType = require('credit-card-type');
var getTypeInfo = require('credit-card-type').getTypeInfo;
var CardType = require('credit-card-type').types;
declare var moment: any;
declare var Stripe: any;
declare var AWS: any;
declare var toastr: any;

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  private _client: Paho.MQTT.Client;

  constructor(
    private _lang: LanguageService,
    private _conf: Configuration,
    private _zone: NgZone,
    private _router: Router,
    private _serviceCheckout: CheckoutService,
    private _service: ProfileService,
    private _missionService: MissionService
  ) {

  }

  accLang: any;
  loaderPage = false;
  loginSignToggle = false;
  savedAddress: any;
  headerrestOpen = false;
  profileName: any;
  email: any;
  mobileno: any;
  addressID: any;
  firstName: string;
  lastName: string;
  newPassword: string;
  rePassword: string;
  oldPassword: string;
  profilePic: string;
  errMsg: any;
  email_Error: any;
  registerSave = false;
  pending: any;
  past: any;
  upcoming: any;

  accountCard = false;
  customerStripeId: any;
  payGetCartList: any;
  monthYear: string;
  Month: string;
  Year: string;
  cardCredit: string;
  cardBrand: string;
  cardcvv: string;
  cardHolder: string;
  validDM = false;
  validCD: any;
  stripeToken: any;
  stripeCardDetails = false;
  errorSaveCart: any;
  PaymentLoader = false;
  promoCode: any;
  grandTotal: any;

  newCardToggle: any;
  recentTrnsToggle: any;
  infowindow: any;
  searchLocationToggle: any;
  orderDetailsToggle: any;
  addressMap: string;
  addressType = 3;
  typeOne = false;
  typeTwo = false;
  typeThree = false;
  otherText = false;
  Others: string;
  latlng: any;
  BookingList: any;
  reasonList: any;
  cancelFalse: any;
  reasonId: any;
  bookingId: any;


  emailVerify: any;
  listProfile: any;
  pwdVerify: any;
  phoneVerify: any;
  firstLastName: any;
  loaderList = false;
  shimmer: any = [];
  orderListMenu = false;
  logStatus: any;

  sId: any;
  OtpNumber: any;
  oneTime = false;

  souceLat: any;
  souceLng: any;
  driverMarker: any;
  BookingTimer: any;
  proTrackId: any;
  timer: any;
  timerRun: any;

  index: any;
  id: any;
  listTran: any;
  pageIndex = 0;
  walletMoney: any;
  addMoney: string;
  cardId: any;
  walletCart = false;
  profileShow = true;

  bInvoice: any;
  bId: any;
  reviewMsg: any;
  ratingList: any = [];
  reviewSuccess: any;
  reviewErr: any;
  popupCart = false;

  ngOnInit() {
    this._missionService.scrollTop(true);
    this.accLang = this._lang.account;
    let stripKey = this._conf.getItem("PublishableKey");
    Stripe.setPublishableKey(stripKey);
    this.placeHolder();
    this.sendGetBookings();
    // let index = this._conf.getItem("acc") == '' ? 1 : this._conf.getItem("acc");
    let index = this._conf.getItem("acc");
    if ((index == "5" || index == "6")) {
      this.walletCart = index == '5' ? true : false;
      this.profileShow = false;
      this.paymentNavMenu(3);
    } else {
      this.profileShow = true;
      let indexs = index == null || '' ? 1 : index;
      this.paymentNavMenu(Number(indexs));
    }
    this._conf.removeItem("acc");
    this.sendGetCart();
    this.sendGetAddress();
    this.getProfile();
    this.getWallet();
    this.mqttConnect();

    $("#profilePhone").intlTelInput({
      autoHideDialCode: true,
      geoIpLookup: (callback) => {
        $.get("https://ipapi.co/json/", () => { }).always((resp) => {
          var countryCode = (resp && resp.country) ? resp.country : "";
          callback(countryCode);
        });
      }, initialCountry: "auto",
      nationalMode: true,
      separateDialCode: true,
      // utilsScript: "https://d2qb2fbt5wuzik.cloudfront.net/ideliver_amgular/js/utils.js"
    });
  }


  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = $(window).scrollTop();
    if (number > 100) {
      this.headerrestOpen = true;
    } else {
      this.headerrestOpen = false;
    }
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  editProfile() {
    $("body").toggleClass("hideHidden");
    this.loginSignToggle = !this.loginSignToggle;
    this.emailVerify = false;
    this.pwdVerify = false;
    this.phoneVerify = false;
    this.oneTime = false;
    this.firstLastName = false;
    this.errMsg = false;
    this.getProfile();
  }

  getProfile() {
    this._service.getProfiles()
      .subscribe((res: any) => {
        console.log("customer/profile", res);
        this.listProfile = res.data;
        this.firstName = res.data.firstName;
        this.lastName = res.data.lastName;
        this.mobileno = res.data.phone;
        this.email = res.data.email;
        this.profilePic = res.data.profilePic;
      });
  }

  uploadFile(e) {
    $(".imgLoader").show();
    var bucket = new AWS.S3({ params: { Bucket: 'dayrunner' } });
    var fileChooser = e.srcElement;
    var file = fileChooser.files[0];
    let date = moment().format('DYMhmsA');
    if (file) {
      var params = { Key: 'keymaing' + date, ContentType: file.type, Body: file };
      bucket.upload(params, (err, data) => {
        console.log(data);
        $(".imgFile").show();
        $(".profileImg").attr("src", data.Location);
        $(".imgLoader").hide();
      });
    }
    return false;
  };

  removeUpload() {
    $(".imgFile").hide();
    $(".profileImg").attr("src", "");
  }

  profileUpdate() {
    var pic = $(".profileImg").attr("src");
    let list = {
      profilePic: pic || "",
      firstName: this.firstName,
      lastName: this.lastName,
      about: this.listProfile.about,
      preferredGenres: "",
      dateOfBirth: this.listProfile.dateOfBirth
    }
    this.loaderList = true;
    this._service.getProfileupdate(list)
      .subscribe((res: any) => {
        this.errMsg = res.message;
        this.loaderList = false;
        console.log("customer/profile", res);
        this.getProfile();
        setTimeout(() => {
          this.editProfile();
        }, 1000);
      }, err => {
        var error = err.error;
        console.log(error.message);
      });
  }

  passwordUpdate() {
    if (this.newPassword == this.rePassword) {
      let list = {
        oldPassword: this.oldPassword,
        newPassword: this.newPassword
      }
      this.loaderList = true;
      this.errMsg = false;
      this._service.getPwdupdate(list)
        .subscribe((res: any) => {
          this.errMsg = res.message;
          this.loaderList = false;
          console.log("customer/password/me", res);
          setTimeout(() => {
            this.editProfile();
          }, 1000);
        }, err => {
          this.loaderList = false;
          var error = err.error;
          this.errMsg = error.message;
          console.log(error.message);
        });
    } else {
      this.loaderList = false;
      this.errMsg = "password not match";
    }
  }

  emailUpdate() {
    let list = {
      userType: 2,
      email: this.email
    }
    this.loaderList = true;
    this.errMsg = false;
    this._service.getEmailupdate(list)
      .subscribe((res: any) => {
        this.errMsg = res.message;
        this.loaderList = false;
        console.log("customer/email", res);
        setTimeout(() => {
          this.editProfile();
        }, 1000);
      }, err => {
        this.loaderList = false;
        var error = err.error;
        this.errMsg = error.message;
        console.log(error.message);
      });
  }

  emailValidation(value) {
    // console.log(value);
    var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

    if (regexEmail.test(value)) {
      this.email_Error = "0";
      this.registerSave = true;
      this.errMsg = false;

    } else {
      this.email_Error = "1";
      if (value.length > 5) {
        this.errMsg = "The email ID is invalid";
      }
      this.registerSave = false;
      // setTimeout(() => {
      //   this.errMsg = false;
      // }, 3000)
    }
  }

  mobileValidation(value) {
    var regexPhone = /^(\+91-|\+91|0)?\d{10}$/;

    if (value.match(regexPhone)) {
      this.registerSave = true;
      this.errMsg = false;
    } else {
      var val = value.replace(/([^+0-9]+)/gi, '')
      this.mobileno = val;
      if (value.length > 5) {
        this.errMsg = "The Mobile number entered is incorrect";
      }
      this.registerSave = false;
      // setTimeout(() => {
      //   this.errMsg = false;
      // }, 3000)
    }
  }

  phoneUpdate() {
    let flag = $("#profilePhone").intlTelInput("getSelectedCountryData");
    let list = {
      userType: 2,
      countryCode: "+" + flag.dialCode,
      phone: this.mobileno
    }
    this.loaderList = true;
    this.errMsg = false;
    this._service.getPhoneupdate(list)
      .subscribe((res: any) => {
        this.errMsg = false;
        this.loaderList = false;
        console.log("customer/phoneNumber", res);
        this.sId = res.data.sid;
        this.oneTime = true;
        this.phoneVerify = false;
        // setTimeout(() => {
        //   this.editProfile();
        // }, 1000);
      }, err => {
        this.loaderList = false;
        var error = err.error;
        this.errMsg = error.message;
        console.log(error.message);
      });
  }

  verifyOtp() {
    this.loaderList = true;
    let list = {
      code: this.OtpNumber,
      userId: this.sId,
      trigger: 3,
      userType: 1
    }
    setTimeout(() => {
      this.loaderList = false;
      this.errMsg = false;
    }, 3000);
    this._service.OtpForgot(list)
      .subscribe((res: any) => {
        this.loaderList = false;
        this.errMsg = res.message;
        setTimeout(() => {
          this.editProfile();
        }, 1000);
      }, (err) => {
        this.loaderList = false;
        var error = err.error;
        this.errMsg = error.message;
        console.log(error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

  resendOtp() {
    this.loaderList = true;
    let list = {
      userId: this.sId,
      trigger: 3,
      userType: 1
    }
    setTimeout(() => {
      this.loaderList = false;
      this.errMsg = false;
    }, 3000);
    this._service.resendOtp(list)
      .subscribe((res: any) => {
        this.loaderList = false;
        this.errMsg = res.message;
        this.OtpNumber = "";
      }, (err) => {
        this.loaderList = false;
        var error = err.error;
        this.errMsg = error.message;
        console.log(error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }



  sendGetBookings() {
    this._service.getBookings()
      .subscribe((res: any) => {
        this.loaderPage = true;
        console.log("customer/bookings", res)
        this.pending = res.data.pending;
        this.past = res.data.past;
        this.upcoming = res.data.upcoming;
        if (this.pending && this.pending.length > 0) {
          this.pending.forEach(x => {
            x.bookingRequestedFor = moment.unix(x.bookingRequestedFor).format("DD MMM, YYYY | hh:mm A");
            x.totalAmount = parseFloat(x.totalAmount).toFixed(2);
            var serverT = x.serverTime;
            var expiryT = x.bookingExpireTime;
            var totalSec = expiryT - serverT;
            // this.countdown(totalSec);
            // var hrs = Math.floor(totalSec / 3600);
            // var mins = Math.floor(totalSec % 3600 / 60);
            // var secs = Math.floor(totalSec % 3600 % 60);
            // x.timing = hrs + ':' + mins + ':' + secs;

            /** time **/
            var seconds = totalSec;
            var interval = setInterval(() => {
              var days = Math.floor(seconds / 24 / 60 / 60);
              var hoursLeft = Math.floor((seconds) - (days * 86400));
              var hours = Math.floor(hoursLeft / 3600);
              var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
              var minutes = Math.floor(minutesLeft / 60);
              var remainingSeconds = seconds % 60;
              if (remainingSeconds < 10) {
                remainingSeconds = 0 + remainingSeconds;
              }
              x.timing = hours + ":" + minutes + ":" + remainingSeconds;
              if (seconds == 0) {
                clearInterval(interval);
                this.sendGetBookings();
                // console.log("Completed");
              } else {
                seconds--;
              }
            }, 1000);
          });
        }
        if (this.past && this.past.length > 0) {
          this.past.forEach(x => {
            x.bookingRequestedFor = moment.unix(x.bookingRequestedFor).format("DD MMM, YYYY | hh:mm A");
            x.totalAmount = parseFloat(x.totalAmount).toFixed(2);
          });
        }
        if (this.upcoming && this.upcoming.length > 0) {
          this.upcoming.forEach(x => {
            x.bookingRequestedFor = moment.unix(x.bookingRequestedFor).format("DD MMM, YYYY | hh:mm A");
            x.totalAmount = parseFloat(x.totalAmount).toFixed(2);
          });
        }
        setTimeout(() => {
          if (this.upcoming[0] && this.upcoming[0].status == 3) {
            $("body").removeClass("hideHidden");
            this.orderDetailsToggle = false;
            $("#2menu").trigger("click");
          }
        }, 500)

      }, err => {
        var error = err.error;
        console.log(error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }





  paymentNavMenu(id: number) {
    this._conf.removeItem("acc")
    $(".list_card").removeClass("active");
    $('#' + id + 'paynavMenu').addClass("active");
    $(".list_card_information").removeClass("active");
    $('#' + id + 'payNavMenu').addClass("active");
  }



  valdationCreditCard(value: any) {
    $(".activeGetCart").removeClass("active");
    this.errorSaveCart = false;
    var visaCards = creditCardType(value);
    var val = value.split(" ").join("");
    if (val.length > 0) {
      val = val.match(new RegExp('.{1,4}', 'g')).join(" ");
    }
    this.cardCredit = val;
    if (value.length <= 17) {
      this.validCD = "The card number should be 15 or 16 digits!";
      // this.stripeCardDetails = true;
    } else {
      this.validCD = false;
      // this.stripeCardDetails = false;
    }

    if (value.length <= 4) {
      if (visaCards != '') {
        // console.log(visaCards[0].type);
        this.cardBrand = visaCards[0].type;
        this.validCD = false;
        this.stripeCardDetails = true;
      } else {
        this.validCD = "invalid card number!";
        this.stripeCardDetails = false;
        this.cardCredit = "";
        console.log("error cardtype");
      }
    }
  }



  valdationDate(val: any) {
    this.errorSaveCart = false;
    this.monthYear = val.replace(/(\d{2}(?!\s))/g, "$1/");

    if (isNaN(val)) {
      val = val.replace(/[^0-9\/]/g, '');
      if (val.split('/').length > 2)
        val = val.replace(/\/+$/, "");
      this.monthYear = val;
    }

    var validDm = /^(0[1-9]|1[0-2])\/\d{4}$/.test(val);
    // console.log(val);
    if (val.length > 2 && validDm == false) {
      this.validDM = true;
      this.stripeCardDetails = false;
    } else {
      this.validDM = false;
      this.stripeCardDetails = true;
    }

    if (val.length == 1) {
      if (val.charAt(0) == 0 || val.charAt(0) == 1) {
        this.monthYear = val;
      } else {
        this.monthYear = '0' + val;
      }
    } else if (val.length == 2) {
      if (val <= 12) {
        this.monthYear = val;
      } else {
        this.monthYear = "";
      }
    }

  }

  valdationcvv(val: any) {
    this.errorSaveCart = false;
    if (val.length == 3) {
      this.stripeCardDetails = true;
    } else {
      this.stripeCardDetails = false;
    }
  }


  stripeCard() {
    // var val = String(Number(this.monthYear));
    var validDateMnth = /^(0[1-9]|1[0-2])\/\d{4}$/.test(this.monthYear);
    // console.log(this.monthYear, validDateMnth);   
    if (this.cardCredit && this.monthYear && this.cardcvv && validDateMnth == true) {
      this.validDM = false;
      this.sendCardStripToken();
    } else {
      this.validDM = true;
      console.log("error stripeCartList");
    }

  }



  sendCardStripToken() {
    this.PaymentLoader = true;
    setTimeout(() => {
      this.PaymentLoader = false;
    }, 5000)
    var MYdetails = this.monthYear.split("/");
    var numberCard = this.cardCredit.split(" ").join("");
    (<any>window).Stripe.card.createToken({
      number: this.cardCredit,
      exp_month: Number(MYdetails[0]),
      exp_year: Number(MYdetails[1]),
      cvc: this.cardcvv
    }, (status: number, response: any) => {
      this._zone.run(() => {
        if (status === 200) {
          this.errorSaveCart = false;
          this.stripeToken = response.id;
          this.sendCardStrip();
          console.log(response.id);
        } else {
          this.errorSaveCart = response.error.message;
          setTimeout(() => {
            this.errorSaveCart = false;
          }, 5000);
          console.log(response.error.message)

        }
      });
    });
  }


  sendCardStrip() {
    var email = this._conf.getItem("email");
    let stripeCartList = {
      cardToken: this.stripeToken,
      email: email
    }
    this._serviceCheckout.paymentStripeCart(stripeCartList)
      .subscribe((res: any) => {
        this.PaymentLoader = false;
        console.log("stripeRes", res);
        $(".modal").modal("hide");
        this.sendGetCart();
        this.resetSaveCart();
        this.cardToggle();
      }, (err) => {
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

  sendGetCart() {
    this._serviceCheckout.paymentGetCart()
      .subscribe((res: any) => {
        this.accountCard = false;
        console.log("paymentget-card", res);
        this.payGetCartList = res.data;
        if (this.payGetCartList && this.payGetCartList.length > 0) {
          this.payGetCartList.forEach(x => {
            x.checked = false;
          });
          this.payGetCartList[0].checked = true;
          this.cardId = this.payGetCartList[0].id;
        }
        // console.log(res)

      }, (err) => {
        console.log("pget", err);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }


  deleteGetCart(i: number, id: any) {
    this.index = i;
    this.id = id;
    $("#cardDelete").modal("show");
  }

  deleteCard() {
    let deletepayGetCart = {
      cardId: this.id
    }
    this._serviceCheckout.paymentGetCartDelete(deletepayGetCart)
      .subscribe((res: any) => {
        this.sendGetCart();
        this.payGetCartList.splice(this.index, 1);
      }, (err) => {
        console.log("pget", err)
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

  selectPayments(i: number) {
    this.payGetCartList.forEach(x => {
      x.checked = false;
    });
    this.payGetCartList[i].checked = true;
    this.cardId = this.payGetCartList[i].id;
  }

  resetSaveCart() {
    this.cardCredit = "";
    this.monthYear = "";
    this.cardcvv = "";
    this.cardHolder = "";
  }

  closeOpenSearch() {
    $("body").toggleClass("hideHidden");
    this.searchLocationToggle = !this.searchLocationToggle;

    let latlng = this._conf.getItem("latlng");
    if ((latlng) && $("body").hasClass("hideHidden")) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      var address = JSON.parse(this._conf.getItem("place"));
    }
    // $("#txtPlaces").val(address);
    this.addressMap = address;
    $(".areaName").val(address);
    $(".arealat").val(this.latlng.lat);
    $(".arealng").val(this.latlng.lng);
    this.typeThree = false;
    this.typeTwo = false;
    this.typeOne = false;
    this.otherText = false;
    this.continueMap();
  }


  editAddress(index) {
    // console.log(this.savedAddress[index])
    $("body").toggleClass("hideHidden");
    this.searchLocationToggle = !this.searchLocationToggle;

    this.addressID = this.savedAddress[index]._id;
    let address1 = this.savedAddress[index].addLine1;
    var latAddress = this.savedAddress[index].latitude;
    var lngAddress = this.savedAddress[index].longitude;
    this.selectType(this.savedAddress[index].taggedType, this.savedAddress[index].taggedAs);
    this.latlng = {
      lat: latAddress,
      lng: lngAddress
    }
    // $("#txtPlaces").val(address1);
    this.addressMap = address1;
    $(".areaName").val(address1);
    $(".arealat").val(this.latlng.lat);
    $(".arealng").val(this.latlng.lng);
    this.continueMap();
  }

  selectType(type: number, val) {
    this.addressType = type;
    if (type == 1) {
      this.typeOne = !this.typeOne;
      this.typeTwo = false;
      this.typeThree = false;
    } else if (type == 2) {
      this.typeTwo = !this.typeTwo;
      this.typeOne = false;
      this.typeThree = false;
    } else if (type == 3) {
      this.Others = val;
      this.otherText = true;
      this.typeThree = !this.typeThree;
      this.typeTwo = false;
      this.typeOne = false;
    }
  }

  saveAddress() {
    this.progressBar();
    this.loaderList = true;
    var address = $(".areaName").val();
    var lat = $(".arealat").val();
    var lng = $(".arealng").val();
    if (this.addressType == 1) {
      var name = "Home";
    } else if (this.addressType == 2) {
      var name = "Office";
    } else if (this.addressType == 3) {
      var name = "Other";
    }
    let list = {
      addLine1: address,
      addLine2: "",
      city: "",
      state: "",
      country: "",
      placeId: "",
      pincode: "",
      latitude: lat,
      longitude: lng,
      taggedAs: name,
      userType: 1
    }
    if (this.Others) {
      list.taggedAs = this.Others;
    }

    if (this.addressID) {
      this._service.addressEditSaved(list, this.addressID)
        .subscribe((res: any) => {
          this.addressID = "";
          this.loaderList = true;
          console.log("edit customer/address", res);
          this.sendGetAddress();
          $("body").toggleClass("hideHidden");
          this.searchLocationToggle = false;
        }, (err) => {
          this.loaderList = true;
          console.log(err.error);
          if (err.status == 498) {
            this._missionService.loginOpen(true);
          }
        });
    } else {
      this._service.addressSaved(list)
        .subscribe((res: any) => {
          this.loaderList = true;
          console.log("customer/address", res);
          this.sendGetAddress();
          $("body").toggleClass("hideHidden");
          this.searchLocationToggle = false;
        }, (err) => {
          this.loaderList = true;
          console.log(err.error);
          if (err.status == 498) {
            this._missionService.loginOpen(true);
          }
        });
    }
  }


  progressBar() {
    this.errMsg = false;
    var progress = setInterval(() => {
      var $bar = $('.barStore');
      if ($bar.width() >= 400) {
        clearInterval(progress);
      } else {
        $bar.width($bar.width() + 5 + "%");
      }
    }, 100);
  }

  totalAmount : any;

  listBooking(list, status) {
    console.log(list)
    this.orderListMenu = false;
    $("body").toggleClass("hideHidden");
    this.orderDetailsToggle = !this.orderDetailsToggle;
    clearInterval(this.timer);
    // console.log(this.timerRun);
    if (list != 0) {
      this.BookingList = list;
      this.BookingList.accounting.total = Number(this.BookingList.accounting.total).toFixed(2);
      (this.BookingList.accounting.totalTaxApplied) ? this.totalAmount = (Number(this.BookingList.accounting.total).toFixed(2)) + (this.BookingList.accounting.totalTaxApplied[0].taxAmount) :
      this.totalAmount = (Number(this.BookingList.accounting.total).toFixed(2));
        console.log("the total amount is:", this.totalAmount )
      this.BookingList.providerStatus = status;
      if (list.additionalService && list.additionalService.length > 0) {
        list.additionalService.forEach(x => {
          x.price = parseFloat(x.price).toFixed(2);
        });
      }
      // console.log(this.BookingList.latitude, this.BookingList.longitude);
    }
    if (status == 1) {
      this.jobLogs(this.BookingList.bookingId);
    }
  }


  jobLogs(id) {
    this._service.getStatus(id)
      .subscribe((res: any) => {
        console.log("customer/booking", res);
        this.logStatus = res.data;
        if (res.data.status == 8 && res.data.bookingTimer) {
          // console.log("test timer")
          if (res.data.bookingTimer.status == 0) {
            clearInterval(this.timer);
            let totalSeconds = res.data.bookingTimer.second;
            var hour = Math.floor(totalSeconds / 3600);
            var minute = Math.floor((totalSeconds - hour * 3600) / 60);
            var seconds = totalSeconds - (hour * 3600 + minute * 60);
            this.BookingTimer = hour + ":" + minute + ":" + seconds;
          } else {
            let time = res.data.serverTime - res.data.bookingTimer.startTimeStamp + res.data.bookingTimer.second;
            this.setTimer(time, res.data.bookingTimer.status);
          }
        }
      }, (err) => {
        console.log(err.error.message)
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }


  mqttConnect() {
    var id = this._conf.getItem('sid');
    this._client = new Paho.MQTT.Client("mqtt.tourangels.it", 2083, "/mqtt", id);

    this._client.onConnectionLost = (responseObject: Object) => {
      console.log('Connection lost.', responseObject);
      // this.mqttConnect();
      // $("body").removeClass("hideHidden");
      // this.orderDetailsToggle = false;
      // this.ngOnInit();
    };

    this._client.onMessageArrived = (message: Paho.MQTT.Message) => {
      if (message.destinationName == 'liveTrack/' + this.proTrackId) {
        console.log("track", JSON.parse(message.payloadString))
        let latlng = JSON.parse(message.payloadString);
        var latlngs = new google.maps.LatLng(latlng.latitude, latlng.longitude);
        this.driverMarker.setPosition(latlngs);
        this.dlat = latlng.latitude;
        this.dlng = latlng.longitude;
        this.distaneTrack();
      } else {
        console.log("jobStatus", JSON.parse(message.payloadString))
        // if (this.proTrackId) {
        //   this._client.unsubscribe("liveTrack/" + this.proTrackId, { qos: 2 });
        // }
        let statusList = JSON.parse(message.payloadString);
        var driverStatus = statusList.data;
        toastr.remove();
        if (driverStatus.status == 11) {
          this.sendGetBookings();
          toastr.error(driverStatus.msg, "Go-tasker");
        } else {
          toastr.info(driverStatus.msg, "Go-tasker");
        }
        // console.log("status", driverStatus.status)
        if (driverStatus.status == 3) {
          this.sendGetBookings();
        }
        if (driverStatus.status == 7) {
          this.orderListMenu = false;
        }
        if (driverStatus.status == 10) {
          this.sendGetBookings();
          this.bId = driverStatus.bookingId;
          this.getReviewList();
          $("body").removeClass("hideHidden");
          this.orderDetailsToggle = false;
        }

        if (driverStatus.status == 8 && driverStatus.bookingTimer && driverStatus.bookingTimer.status == 0) {
          clearInterval(this.timer);
        }
        if (driverStatus.status == 8 && driverStatus.bookingTimer) {
          this.jobLogs(driverStatus.bookingId);
        } else if (driverStatus.status != 8) {
          this.jobLogs(driverStatus.bookingId);
        }

      }
    };

    this._client.connect({ onSuccess: this.onConnected.bind(this) });
  }

  private onConnected(): void {
    console.log('Connected to broker sid.');
    var cId = this._conf.getItem("sid");
    this._client.subscribe("jobStatus/" + cId, { qos: 2 });
  }

  setTimer(second, status) {
    // console.log(second, status);
    var totalSeconds = second;
    this.timer = setInterval(() => {
      ++totalSeconds;
      var hour = Math.floor(totalSeconds / 3600);
      var minute = Math.floor((totalSeconds - hour * 3600) / 60);
      var seconds = totalSeconds - (hour * 3600 + minute * 60);
      this.BookingTimer = hour + ":" + minute + ":" + seconds;
    }, 1000)
  }

  cancelBookingReason(id) {
    this.errMsg = false;
    this.bookingId = id;
    this._service.cancelReason(id)
      .subscribe((res: any) => {
        this.cancelFalse = false;
        console.log("cancelReasons", res);
        this.reasonList = res;
        $("#cancelReason").modal("show");
      })
  }


  bookingCanceled() {
    if (this.reasonId) {
      this.errMsg = false;
      let list = {
        bookingId: this.bookingId,
        resonId: this.reasonId
      }
      this._service.cancelReasons(list)
        .subscribe((res: any) => {
          console.log("customer/cancelBooking", res);
          // this.reasonList = res;
          toastr.error(res.message, "Go-tasker");
          $("#cancelReason").modal("hide");
          $("body").removeClass("hideHidden");
          this.orderDetailsToggle = false;
          this.sendGetBookings();
          $("#3menu").trigger("click");
        })
    } else {
      this.errMsg = "Select any one reason";
    }

  }

  bookingCancel(id) {
    this.errMsg = false;
    this.reasonId = id;
  }




  liveTractMqtt(lat, lng, id) {
    this.slat = lat;
    this.slng = lng;
    this.proTrackId = id;
    this.orderListMenu = true;
    if (this.proTrackId) {
      this._client.subscribe("liveTrack/" + this.proTrackId, { qos: 2 });
    }

    var lineSymbol = {
      path: google.maps.SymbolPath.CIRCLE,
      fillOpacity: 1,
      scale: 3
    };
    this.directionsService = new google.maps.DirectionsService();
    this.directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#01b5f6",
        strokeWeight: 3,
        strokeOpacity: 1,
        // fillOpacity: 0,
        // icons: [{
        //   icon: lineSymbol,
        //   offset: '0',
        //   repeat: '10px'
        // }],
      }, suppressMarkers: true
    });

    this.map = new google.maps.Map(document.getElementById('liveTrackMap'), {
      center: { lat: lat, lng: lng },
      zoom: 12,
      disableDefaultUI: false
    });
    this.directionsDisplay.setMap(this.map);

    let proImg = this.logStatus.providerDetail.profilePic;
    // $("img[src='" + proImg + "']").css({ "border-radius": "50%", "border": "2px solid #01b5f6" });
    var iconDest = {
      url: proImg, // url
      scaledSize: new google.maps.Size(40, 40), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0), // anchor     
    };


    var iconSource = {
      url: "assets/images/source.png", // url
      scaledSize: new google.maps.Size(30, 30), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    };

    this.infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: { lat: lat, lng: lng },
      map: this.map,
      // icon: iconSource
    });

    this.driverMarker = new google.maps.Marker({
      position: { lat: 0.000, lng: 0.000 },
      map: this.map,
      icon: iconDest
    });
  }

  dlat: any;
  dlng: any;
  slat: any;
  slng: any;
  map: any;
  directionsService: any;
  directionsDisplay: any;

  distaneTrack() {
    // var service = new google.maps.DistanceMatrixService();
    // service.getDistanceMatrix(
    //   {
    //     origins: [{ lat: this.slat, lng: this.slng }],
    //     destinations: [{ lat: this.dlat, lng: this.dlng }],
    //     travelMode: 'DRIVING',
    //     unitSystem: google.maps.UnitSystem.IMPERIAL,
    //   }, (response, status) => {        
    //     console.log(response, status);
    //     let dist = response.rows[0].elements[0];
    //     this.logStatus.distanceMatrix = this.BookingList.distanceMatrix;
    //     this.logStatus.distance = dist.distance.value;
    //     this.logStatus.eta = dist.duration.text;
    //   });

    // var flightPlanCoordinates = [
    //   { lat: this.slat, lng: this.slng }
    // ];
    // flightPlanCoordinates[1] = { lat: this.dlat, lng: this.dlng };
    // console.log(flightPlanCoordinates)
    // var flightPath = new google.maps.Polyline({
    //   path: flightPlanCoordinates,
    //   geodesic: true,
    //   strokeColor: '#01b5f6',
    //   strokeOpacity: 1.0,
    //   strokeWeight: 2
    // });
    // flightPath.setMap(this.map);

    // var directionsService = new google.maps.DirectionsService();
    // var directionsDisplay = new google.maps.DirectionsRenderer({
    //   polylineOptions: {
    //     strokeColor: "#01b5f6",
    //     strokeWeight: 3,
    //     strokeOpacity: 1,
    //   }, suppressMarkers: true
    // });
    // var startMarker = new google.maps.Marker({ position: start, map: this.map, icon: 'assets/images/source.png' });
    // var stopMarker = new google.maps.Marker({ position: stop, map: this.map, icon: 'assets/images/pin.png' });
    let proImg = this.logStatus.providerDetail.profilePic;
    $(".mapList img[src='" + proImg + "']").css({ "border-radius": "50%", "border": "2px solid #01b5f6" });
    var start = new google.maps.LatLng(this.slat, this.slng);
    var end = new google.maps.LatLng(this.dlat, this.dlng);
    var request = {
      origin: start,
      destination: end,
      travelMode: google.maps.TravelMode.DRIVING
    };
    this.directionsService.route(request, (response, status) => {
      // console.log(response, status)
      if (status == google.maps.DirectionsStatus.OK) {
        var leg = response.routes[0].legs[0];
        this.logStatus.distanceMatrix = this.BookingList.distanceMatrix;
        this.logStatus.distance = leg.distance.value;
        this.logStatus.eta = leg.duration.text;
        // this.makeMarker(leg.start_location, iconSource,  this.logStatus.eta);
        // this.makeMarker(leg.end_location, iconDest,  this.logStatus.eta);
        this.directionsDisplay.setDirections(response);
        // directionsDisplay.setMap(this.map);
      } else {
        console.log("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
      }
    });
  }

  makeMarker(position, icon, title) {
    new google.maps.Marker({
      position: position,
      map: this.map,
      icon: icon,
      title: title
    });
  }


  sendGetAddress() {
    this.Others = "";
    this.addressID = "";
    this._service.getAddress()
      .subscribe((res: any) => {
        console.log("customer/address", res);
        this.savedAddress = res.data;
        this.savedAddress.forEach(x => {
          x.taggedAs = x.taggedAs.toLowerCase();
          if (x.taggedAs == 'home') {
            x.taggedType = 1;
          } else if (x.taggedAs == 'office' || x.taggedAs == 'work') {
            x.taggedType = 2;
          } else {
            x.taggedType = 3;
          }
        });
      }, (err) => {
        console.log(err.error);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

  deleteAddressList(addressID: any, i: any) {
    $("#deleteAddress").modal("show");
    this.addressID = addressID;
    this.index = i;
  }

  deleteAddress() {
    this._service.addressDelete(this.addressID)
      .subscribe((result: any) => {
        this.Others = "";
        this.addressID = "";
        this.savedAddress.splice(this.index, 1);
        $(".modal").modal("hide");
      }, (err) => {
        var error = JSON.parse(err._body);
        // console.log(error)
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      });
  }

  cardToggle() {
    $("body").toggleClass("hideHidden");
    this.newCardToggle = !this.newCardToggle;
  }

  recentTrns() {
    $("body").toggleClass("hideHidden");
    this.recentTrnsToggle = !this.recentTrnsToggle;
  }

  trasactionWallet(i) {
    $(".titleT").removeClass("active");
    $("#" + i + "t").addClass("active");
    $("#" + i + "T").addClass("active");
  }


  getAllTransaction() {
    let list = {
      pageIndex: this.pageIndex
    }
    this._service.getTransaction(list)
      .subscribe((res: any) => {
        console.log("wallet/transction", res);
        this.listTran = res.data;
        if (res.data.creditDebitArr && res.data.creditDebitArr.length > 0) {
          res.data.creditDebitArr.forEach(y => {
            y.dateTime = moment.unix(y.timestamp).format("DD MMM, YYYY | hh:mm A");
            y.amount = parseFloat(y.amount).toFixed(2);
          });
        }
        if (res.data.debitArr && res.data.debitArr.length > 0) {
          res.data.debitArr.forEach(y => {
            y.dateTime = moment.unix(y.timestamp).format("DD MMM, YYYY | hh:mm A");
            y.amount = parseFloat(y.amount).toFixed(2);
          });
        }
        if (res.data.creditArr && res.data.creditArr.length > 0) {
          res.data.creditArr.forEach(y => {
            y.dateTime = moment.unix(y.timestamp).format("DD MMM, YYYY | hh:mm A");
            y.amount = parseFloat(y.amount).toFixed(2);
          });
        }

      }, err => {
        console.log(err.error.message)
      });
  }

  getWallet() {
    this.getAllTransaction();
    this._service.getwalletMoney()
      .subscribe((res: any) => {
        console.log("customer/wallet", res);
        this.walletMoney = res.data;
        this._missionService.addWallets(true);
      }, err => {
        console.log(err.error.message)
      });
  }

  addWallet() {
    if (this.cardId) {
      let list = {
        cardId: this.cardId,
        amount: this.addMoney
      }
      setTimeout(() => {
        this.errMsg = false;
        this.PaymentLoader = false;
      }, 5000);
      if (this.addMoney) {
        this.PaymentLoader = true;
        this.errMsg = false;
        this._service.getaddwalletMoney(list)
          .subscribe((res: any) => {
            console.log("wallet/recharge", res);
            $(".modal").modal("hide");
            this.PaymentLoader = false;
            // this.errMsg = res.message;
            this.getWallet();
            this.addMoney = "";
          }, err => {
            this.PaymentLoader = false;
            console.log(err.error.message);
            this.errMsg = err.error.message;
          });
      } else {
        this.errMsg = "Please enter the amount to continue."
      }
    } else {
      $(".modal").modal("hide");
      this.errorSaveCart = "Please add a card";
      this.cardToggle();
    }
  }


  profileEdit() {
    this.oneTime = false;
    this.emailVerify = false;
    this.pwdVerify = false;
    this.phoneVerify = false;
    this.firstLastName = true;
  }

  phoneEdit() {
    this.oneTime = false;
    this.emailVerify = false;
    this.pwdVerify = false;
    this.phoneVerify = true;
    this.firstLastName = false;
  }

  emailEdit() {
    this.oneTime = false;
    this.emailVerify = true;
    this.pwdVerify = false;
    this.phoneVerify = false;
    this.firstLastName = false;
  }

  pwdEdit() {
    this.oneTime = false;
    this.emailVerify = false;
    this.pwdVerify = true;
    this.phoneVerify = false;
    this.firstLastName = false;
  }
  getReviewList() {
    let list = {
      bId: this.bId
    }
    this._service.getReview(list)
      .subscribe((res: any) => {
        console.log("booking/invoice", res);
        this.bInvoice = res.data;
        if (this.bInvoice.customerRating) {
          this.bInvoice.customerRating.forEach(x => {
            x.rating = 5;
          });
        }
        if (this.bInvoice.additionalService && this.bInvoice.additionalService.length > 0) {
          this.bInvoice.additionalService.forEach(x => {
            x.price = parseFloat(x.price).toFixed(2);
          });
        }
        this.bId = res.data.bookingId;
        res.data.bookingRequestedFor = moment.unix(res.data.bookingRequestedFor).format("DD MMM, YYYY | hh:mm A");
        res.data.accounting.total = parseFloat(res.data.accounting.total).toFixed(2);
        (res.data.accounting.totalTaxApplied) ? this.totalAmount = (res.data.accounting.total) + (res.data.accounting.totalTaxApplied[0].taxAmount) :
        this.totalAmount = (res.data.accounting.total)
        this.reviewSuccess = false;
        $("#reviewProvider").modal("show");
        this.popupCart = false;
      }, err => {
        console.log(err.error.message)
      });
  }



  show_rating_txt(val, list, i) {
    // console.log(this.bInvoice.customerRating[i])
    this.bInvoice.customerRating[i].rating = val;
    // this.reviewErr = false;
    // let ratingList = {
    //   _id: list._id,
    //   name: list.name,
    //   rating: val
    // }
    // let itemIndex = this.ratingList.findIndex(x => x._id == list._id);
    // if (itemIndex > -1) {
    //   this.ratingList[itemIndex].rating = val;
    // } else {
    //   this.ratingList.push(ratingList);
    // }
    // console.log(this.ratingList)
  }


  reviewSubmitList() {
    this.ratingList = [];
    if (this.bInvoice.customerRating) {
      this.bInvoice.customerRating.forEach(x => {
        let ratingList = {
          _id: x._id,
          name: x.name,
          rating: x.rating
        }
        this.ratingList.push(ratingList);
      });
    }

    if (this.ratingList && this.ratingList.length > 0) {
      let list = {
        bookingId: this.bId,
        rating: this.ratingList,
        review: this.reviewMsg
      }
      this.loaderList = true;
      this._service.reviewSubmit(list)
        .subscribe((res: any) => {
          console.log("booking/invoice", res);
          this.loaderList = true;
          this.reviewSuccess = true;
          setTimeout(() => {
            $(".modal").modal("hide");
            this._router.navigate([""]);
          }, 2500)
        });
    } else {
      this.reviewErr = "Please rate";
    }
  }

  closeLocation() {
    // $("#txtPlaces").val("");
    this.addressMap = "";
  }


  changeLocation() {
    var input = document.getElementById('txtPlaces');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener('place_changed', () => {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === 'locality') {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
          }
          if (addressObj.types[j] === 'administrative_area_level_1') {
            var state = addressObj.short_name;
            // console.log(addressObj.short_name);
          }
        }
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      this.latlng = {
        lat: lat,
        lng: lng
      }
      $(".areaName").val(place.formatted_address);
      $(".arealat").val(lat);
      $(".arealng").val(lng);
    });
  }

  locationChange() {
    setTimeout(() => {
      this.continueMap();
    }, 500);
  }


  continueMap() {
    var latlng = this.latlng;
    var map = new google.maps.Map(document.getElementById('mapData'), {
      center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
      zoom: 15,
      disableDefaultUI: true,
      mapTypeControl: true
    });

    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlng);
      var marker = new google.maps.Marker({
        map: map,
      });
    }, 300);

    this.infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: true
    });

    var latlng = new google.maps.LatLng(latlng.lat, latlng.lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          // $(".addressArea").val(results[0].formatted_address);
          // $(".areaName").val(results[0].formatted_address);
          // $(".arealat").val(latlng.lat);
          // $(".arealng").val(latlng.lng);
        }
      }
    });


    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, 'dragend', () => {

      geocoder.geocode({ 'latLng': marker.getPosition() }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            this.addressMap = results[0].formatted_address;
            $(".addressArea").val(results[0].formatted_address);
            $(".areaName").val(results[0].formatted_address);
            $(".arealat").val(marker.getPosition().lat());
            $(".arealng").val(marker.getPosition().lng());
            this.infowindow.setContent(results[0].formatted_address);
            this.infowindow.open(map, marker);
          }
        }
      });
    });
  }


}
