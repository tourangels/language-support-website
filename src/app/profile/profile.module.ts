import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { SharedLazyModule } from '../shared/shared-lazy.module';

const routes: Routes = [  
  { path: '', component: ProfileComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedLazyModule
  ],
  declarations: [
    ProfileComponent
  ]
})
export class ProfileModule { }
