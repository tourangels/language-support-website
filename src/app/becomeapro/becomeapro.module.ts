import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TagInputModule } from 'ngx-chips';
import { RouterModule, Routes } from '@angular/router';
import { BecomeaproComponent } from './becomeapro.component';
import { SharedLazyModule } from '../shared/shared-lazy.module';
const routes: Routes = [  
  { path: '', component: BecomeaproComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TagInputModule,
    SharedLazyModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    BecomeaproComponent 
  ]
})
export class BecomeaproModule { }
