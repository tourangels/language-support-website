import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BecomeaproComponent } from './becomeapro.component';

describe('BecomeaproComponent', () => {
  let component: BecomeaproComponent;
  let fixture: ComponentFixture<BecomeaproComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BecomeaproComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BecomeaproComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
